#This files contains necessary functions for the analysis of the iBM simulations

require(plyr) #necessary for data processing
require(gdata) #necessary to read xls files
require(stringr)
require(data.table)
require(dplyr)
require(ggplot2)
require(cowplot)
require(tidyverse)
require(ggbeeswarm)
source('~/Documents/Projects/Malaria/modeling/ABM/src/RScripts/ggplotThemes.R')

##necessary functions ----
readDataFrames<-function(df.Name, type)
{
  tmp<-fread(df.Name)
  Setting<-rep(type, nrow(tmp))
  out<-data.frame(tmp,Setting)
  return(out)
}

binTime<-function(x)
{
  out<-c()
  for(i in x)
  {
    if(i<=100)
    {
      tmp<-"<100"
      out<-c(out,tmp)
    }
    if (i>100 & i <=200)
    {
      tmp<-"100-200"
      out<-c(out,tmp)
    }
    if (i>200 & i <=300)
    {
      tmp<-"200-300"
      out<-c(out,tmp)
    }
    if (i>300 & i <=400)
    {
      tmp<-"300-400"
      out<-c(out,tmp)
    }
    if (i>400 & i <=500)
    {
      tmp<-"400-500"
      out<-c(out,tmp)
    }
    if (i>500 & i <=600)
    {
      tmp<-"500-600"
      out<-c(out,tmp)
    } 
    if (i>600 & i <=700)
    {
      tmp<-"600-700"
      out<-c(out,tmp)
    }
    if (i>700 & i <=800)
    {
      tmp<-"700-800"
      out<-c(out,tmp)
    }
    if (i>800 & i <=900)
    {
      tmp<-"800-900"
      out<-c(out,tmp)
    }
    if (i>900 & i <=1000)
    {
      tmp<-"900-1000"
      out<-c(out,tmp)
    }
    if (i>1000 & i <=1300)
    {
      tmp<-"1000-1300"
      out<-c(out,tmp)
    }
    if (i>1300 & i <=1500)
    {
      tmp<-"1300-1500"
      out<-c(out,tmp)
    }
    if (i>1500 & i <=1700)
    {
      tmp<-"1500-1700"
      out<-c(out,tmp)
    } 
    if (i>1700 & i <=2000)
    {
      tmp<-"1700-2000"
      out<-c(out,tmp)
    } 
    if (i>2000 & i <=2300)
    {
      tmp<-"2000-2300"
      out<-c(out,tmp)
    }
    if (i>2300 & i <=2500)
    {
      tmp<-"2300-2500"
      out<-c(out,tmp)
    }
    if (i>2500 & i <=2800)
    {
      tmp<-"2500-2800"
      out<-c(out,tmp)
    }
    if (i>2800 & i <=3000)
    {
      tmp<-"2800-3000"
      out<-c(out,tmp)
    }
    if (i>3000 & i <=3200)
    {
      tmp<-"3000-3200"
      out<-c(out,tmp)
    } 
    if (i>5000 & i <=5200)
    {
      tmp<-"5000-5200"
      out<-c(out,tmp)
    } 
    if (i>6900 & i <7000)
    {
      tmp<-"6900-7000"
      out<-c(out,tmp)
    } 
    if (i>7900 & i <8000)
    {
      tmp<-"7900-8000"
      out<-c(out,tmp)
    } 
    if (i>9900)
    {
      tmp<-"9000-10000"
      out<-c(out,tmp)
    } 
  }
  return(out)
}

summarizeBMDistribution<-function(df)
{
  out<-ddply(df, .(Setting, sim_ID, newTime), function(X){
    df<-as.data.frame(table(X$ParasiteBM))
    tmp<-sum(df$Freq)
    total<-rep(tmp, nrow(df))
    Freq2<-df$Freq/sum(df$Freq)
    Time<-X$Time[1]
    data.frame(df,total, Time, Freq2)
  })
  out$Var1<-as.numeric(as.character(out$Var1))
  return(out)
}

summarizeAgeDistribution<-function(df)
{
  out<-ddply(df, .(Setting, sim_ID, newTime), function(X){
    df<-as.data.frame(table(X$Age))
    tmp<-sum(df$Freq)
    total<-rep(tmp, nrow(df))
    Freq2<-df$Freq/sum(df$Freq)
    Time<-X$Time[1]
    data.frame(df,total, Time, Freq2)
  })
  out$Var1<-as.numeric(as.character(out$Var1))
  return(out)
}

plotBMDistribution<-function(df,time = FALSE)
{
  plot_out<-ggplot(df, aes(x = Var1, y = Freq2, colour = Setting, fill = Setting))+
    stat_summary(fun.y = mean, geom = "bar", alpha = 0.4) + 
    stat_summary(fun.y = mean, geom = "point") + 
    stat_summary(fun.data = mean_se, geom = "errorbar", width = 0.25)+
    #stat_summary(fun.data = mean_sdl, geom = "errorbar", width = 0.3,fun.args = list(mult = 1))+
    scale_color_viridis_d(end = 0.5, name = "")+
    scale_fill_viridis_d(end = 0.5, name = "")+
    xlab("BM during oocyst development")+
    ylab("Abundance (%)")+
    theme_bw(base_size = 18)+
    theme(legend.position = "none")
  
  if(isTRUE(time))
  {
    pl<-plot_out +facet_grid(Setting~newTime)
  }else{
    pl<-plot_out +facet_grid(~Setting)
  }
  return(pl)
}

plotAgeDistribution<-function(df, time = FALSE)
{
  plot_out<-ggplot(df, aes(x = Var1, y = Freq2, colour = Setting, fill = Setting))+
    stat_summary(fun.y = mean, geom = "bar", alpha = 0.4) + 
    stat_summary(fun.y = mean, geom = "point") + 
    stat_summary(fun.data = mean_se, geom = "errorbar", width = 0.25)+
    #stat_summary(fun.data = mean_sdl, geom = "errorbar", width = 0.3,fun.args = list(mult = 1))+
    scale_color_viridis_d(end = 0.5,name = "")+
    scale_fill_viridis_d(end = 0.5, name = "")+
    xlab("Mosquito life span (days)")+
    ylab("Abundance (%)")+
    theme_bw(base_size = 18)+
    theme(legend.position = "none")
  
  if(isTRUE(time))
  {
    pl<-plot_out+facet_grid(Setting~newTime)
  }else{
    pl<-plot_out+facet_grid(~Setting)
  }
  return(pl)
}

plotEIR<-function(df)
{
  pl<-ggplot(df, aes(x = Time, y = EIP_m, colour = Setting, fill = Setting))+
    stat_summary(fun.y = 'mean', geom = 'line')+
    stat_summary(fun.data = 'mean_sdl', fun.args = list(mult = 1), geom = 'ribbon', alpha = 0.2, colour = "NA")+
    xlab ("Days")+
    ylab("Sporogonic development (Tsp)")+
    theme_bw(base_size = 18)+
    scale_color_viridis_d(end = 0.5, name = "")+
    scale_fill_viridis_d(end = 0.5,name = "" )+
    guides(fill=FALSE)
  
  return(pl)
}

plotPopSize<-function(df)
{
  pl<-ggplot(df,aes(x = Time, y = 100*totalInfected/numberIndidivudals, colour = Setting, fill = Setting)) +
    stat_summary(fun.y = 'mean', geom = 'line')+
    stat_summary(fun.data = 'mean_sdl',fun.args = list(mult = 1), geom = 'ribbon', alpha = 0.2)+
    xlab ("Days")+ ylab("Infected individuals (%)")+
    theme_bw(base_size = 18)+
    theme(legend.position = "none")+
    scale_color_viridis_d(end = 0.5, name = "")+
    scale_fill_viridis_d(end = 0.5,name = "" )+
    guides(fill=FALSE)+
    facet_wrap(~host)
  
  return(pl)
}

