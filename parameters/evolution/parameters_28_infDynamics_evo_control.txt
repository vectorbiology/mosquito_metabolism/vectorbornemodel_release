#### Unless specified otherwise, all values must be given in terms of probabilities, even those referred to as "rates". Make sure you calculate the desired rates into the appropriate probabilities

#[simulation]
timeStep = 1
timeEnd = 10000
timeInfection = 1500
timeMutation = 5000
initialInfection = true  #should the host population be initialized with a Malaria reservoir population?"
individualEffects = true #should the the random individual effects for larvae carrying capacity be considered?
metabolism = false #should the within-vector metabolic processes be considered?

transmissionProbability = 0.7
temperatureProfile = false #"should the temperature profile be considered?
temperature = 28 #temperature in Celsius (in case no profile is used)
tau = 0.15 #period in the season where no growth occurs

#[general]
Biomass = 5540
eta_Biomass = 0.639 #individual random effects
K = 1000

#[larva]  
larva.growthRate = 1.19
eta_larva.growthRate = 0.513

#parameters for deathRate
a.d = 0.00288 
eta_a.d = 1.12
b.d = 0.00663
eta_b.d = 0.645
c.d = 0.0381
eta_c.d = 0.701

#parameters for pupationTime
m.t = 9.63
eta_m.t = 0.104
k.t = 0.0996
eta_k.t = 0.0258
c.t = 2.95
eta_c.t = 0.188
 
larva.initialPopulation = 100

#[human]
human.birthRate = 0.5
human.deathRate = 1
human.initialPopulation = 1000
human.recoveryRate = 0.5

#[mosquito]
mosquito.deathRate = 1 # this is the maximal daily probability of a death event happening
mosquito.birthRate = 0.03 # taken from Arifin.etal 2013
mosquito.bitingRate = 0.97 # taken from Arifin.etal 2013
shape.gompertz = 0.159 # rate and shape parameters for the gompertz survival distribution. Determined after fitting to S1 mosquito data
rate.gompertz = 0.007
mosquito.initialPopulation = 100

#[parasite]
parasite.developmentHuman.mean = 28
parasite.developmentHuman.sd = 0.25
parasite.developmentMosquito.mean = 13
parasite.developmentMosquito.sd = 0.0
parasite.parasitemia = 0.0001
parasite.reservoirProbability = 0.005
parasite.reservoirVirulence = 0#0.00005
parasite.mutationRate = 0.5
