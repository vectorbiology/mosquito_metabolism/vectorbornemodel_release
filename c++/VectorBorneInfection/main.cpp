//
//  main.cpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#include <iostream>
#include "World.hpp"

int main(int argc, const char * argv[]) {
    std::cout << "Hello, welcome to the simulation of plasmodium tansmission!\n";

    boost::program_options::options_description desc("Allowed options");
    desc.add_options()
    ("-help", "USAGE: -f parameterfile -o output directory")
    ("-f", boost::program_options::value<string>(), "file with the parameter values")
    ("-o", boost::program_options::value<string>(), "directory where the results should be saved");
    
    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

    if (vm.count("-help"))
    {
        cout << desc << "\n";
        return 1;
    }
    if(vm.count("-o"))
    {
        string dir = vm["-o"].as<string>();
        Parameters::outDir = dir;
    }
    if (vm.count("-f"))
    {
        string parameterFile = vm["-f"].as<string>();
        cout << "saving results to: " << Parameters::outDir <<endl;
        Parameters::readConfigFile(parameterFile);
        World myWorld;
        myWorld.createWorld();
        myWorld.simulate();
        
    }
    
    std::cout << "The simulation was successful. Good bye!\n";
    
    return 0;
}
