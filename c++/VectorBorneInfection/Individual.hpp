//
//  Individual.hpp -> Node base class: interface to Visitor
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#ifndef Individual_hpp
#define Individual_hpp

#include <stdio.h>
#include <fstream>
using namespace std;

//forward declaration
class Visitor;
//class Parasite;

class Individual
{
public:

    Individual();
    virtual ~Individual(){};
    
    int getAge(){return this->age;};
    void setAge(int _age){this->age = _age;};
    
    int getGeneration(){return this->generation;};
    void setGeneration(int _generation){this->generation = _generation;};
    
    void setDead(){this->dead = true;};
    bool isDead(){return this->dead;};
    
    void initializeParameters();
    void updateParameters();
    void reportParameters(fstream& outfile, double _simulationTime);
    
    unsigned long int getID(){return this->ID;};
    void setID(unsigned long int _id){this->ID = _id;};
    
    virtual void accept(class Visitor*) = 0;
    
    double getInfectionTime(){return infectionTime;};
    void setInfectionTime(double _time){this->infectionTime = _time;};
    
    double getExposureTime(){return exposureTime;};
    void setExposureTime(double _time){this->exposureTime = _time;};
    
    double getRecoveryTime(){return recoveryTime;};
    void setRecoveryTime(double _time){this->recoveryTime = _time;};
    
  //  void infectIndividual(Parasite& parasite);
    
    enum state{susceptible, exposed, infected, recovered, reservoir};
    void setInfectionState(const string state);
    
    bool isSusceptible(){return (infectionState == susceptible);};
    bool isExposed(){return (infectionState == exposed);};
    bool isInfected(){return (infectionState == infected);};
    bool isRecovered(){return (infectionState == recovered);};
    bool isReservoir(){return (infectionState == reservoir);};
    
    
    Individual& Copy(Individual& rhs);
    
protected:
    
    unsigned long int ID;
    int age;
    int generation;

    bool dead;
    
    double infectionTime;
    double exposureTime;
    double recoveryTime;
    
    state infectionState;

};


#endif /* Individual_hpp */
