//
//  Parasite.cpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#include "Parasite.hpp"
Parasite::Parasite()
{
    mutationRate = 0.0;
    human_virulence = 0.0;
    mosquito_virulence = 0.0;
    incubationPeriodMosquito = 0.0;
    bloodmeals = 0.0;
    strength = 1.;
    value = 0.0;
    p0 = 0.0;
}

//perform a deep copy of a parasite
Parasite& Parasite :: Copy(Parasite& rhsParasite)
{
    if(this == &rhsParasite)
        return *this;
    
    //copying member variables
    this->mutationRate = rhsParasite.mutationRate;
    this->human_virulence = rhsParasite.human_virulence;
    this->mosquito_virulence = rhsParasite.mosquito_virulence;
    this->incubationPeriodMosquito = rhsParasite.incubationPeriodMosquito;
    this->value = rhsParasite.value;
    this->bloodmeals = rhsParasite.bloodmeals;
    this->strength = rhsParasite.strength;
    this->p0 = rhsParasite.p0;
    this->getDNA() = rhsParasite.getDNA().Copy(rhsParasite.getDNA());
    
    return *this;
}

void Parasite::resetParasite()
{
    this->human_virulence = 0.0;
    this->mutationRate = 0.0;
    this->mosquito_virulence = 0.0;
    this->value = 0.0;
    this->bloodmeals = 0;
    //this->strength = 0.0;
    //this->p0 = 0.0;
    //this->incubationPeriodMosquito = 0.0;
}

void Parasite::mutateEIP()
{
    if (randomNumberDouble()<0.5)
        this->incubationPeriodMosquito =this->incubationPeriodMosquito + 0.5;
    else
        this->incubationPeriodMosquito = this->incubationPeriodMosquito - 0.5;
    
    this->getDNA().setDNAID(this->incubationPeriodMosquito);
}

void Parasite::mutateStrength()
{
    if (randomNumberDouble()<0.5)
        this->strength =this->strength + 0.1;
    else
        this->strength =this->strength - 0.1;
}

void Parasite::setIncubationPeriodMosquito(double _number)
{
    this->incubationPeriodMosquito = _number;
    this->getDNA().setDNAID(_number);
}
