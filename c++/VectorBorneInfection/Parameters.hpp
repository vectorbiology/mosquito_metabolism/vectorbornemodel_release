//
//  Parameters.hpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 08.09.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#ifndef Parameters_hpp
#define Parameters_hpp

#include <stdio.h>
#include "MathFunctions.h"
#include "Interface.hpp"

namespace Parameters {
    
    extern string outDir;
    //general simulation parameters
    extern double timeStep;
    extern double timeEnd;
    extern double timeOfMutation;
    extern bool initialInfection;
    extern bool individualEffects;
    extern bool metabolism;
    
    // general biological parameters
    extern double BIOMASS; //carrying capacity larvae
    extern double eta_BIOMASS; //carrying capacity larvae
    extern double K; //carrying capacity humans
    
    extern vector<int> age;
    
    
    //Larva parameters
    //coeeficients of the quadratic death rate function and their individual random effects
    extern double a_d;
    extern double b_d;
    extern double c_d;
    extern double eta_a_d;
    extern double eta_b_d;
    extern double eta_c_d;

    //coeeficients of the quadratic time of pupation function and their individual random effects
    extern double m_t;
    extern double k_t;
    extern double c_t;
    extern double eta_m_t;
    extern double eta_k_t;
    extern double eta_c_t;

    extern double eta_larvaDeathRate; // individual random effect for the death rate
    extern double larvaGrowthRate;
    extern double eta_larvaGrowthRate;  // individual random effect for the growth rate
    extern double larvaPopulationSize;
    
    //Environment parameters
    extern vector<double> temperatureProfile; //periodic function modulating field temperature
    
    //Mosquito parameters
    extern vector<double> ageMosquitoDeathRates; //(age dependent)
    extern double mosquitoDeathRate;
    
    extern double bitingRate;
    extern double mosquitoBirthRate; //probability of the event happening per day
    extern double mosquitoPopulationSize;
    extern double shapeGompertz; //parameters for the gomperrz distribution to calculate mosquito survival (specifically hazard)
    extern double rateGompertz;
   
    //Human parameters   
    extern vector<double> ageHumanDeathRates; // age-dependent
    extern vector<double> ageHumanBirthRates; //age -dependent
    
    extern double humanBirthRate; //probability of the event happening per day
    extern double humanDeathRate; //probability of the event happening per day
    extern double humanPopulationSize;
    
    extern double maxRecoveryRate;
    
    //Parasite parameters
    extern double meanParasiteDevelopmentInHuman;
    extern double meanParasiteDevelopmentInMosquito;
    
    extern double sdParasiteDevelopmentInHuman;
    extern double sdParasiteDevelopmentInMosquito;
    extern vector<double> incubationPeriodMosquitoes;
    
    extern double parasitemia;
    extern double reservoirProbability;
    extern double parasitemiaReservoir;
    extern double parasiteMutationRate;
    
    //Infection Parameters
    extern double timeToStart;
    extern double transmissionProbability;
    
    //Environment Parameters
    extern bool seasonality;
    extern double temperature;

    
    void readConfigFile(const string name);
    void saveParametersFromConfigFile(variables_map vm);
    void printParameters();
    void printIncubationPeriod(const string path);
    
}




#endif /* Parameters_hpp */
