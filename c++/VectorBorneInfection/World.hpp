//
//  World.hpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#ifndef World_hpp
#define World_hpp

#include "Host.hpp"
#include <stdio.h>


class World
{
public:
    World();
    virtual ~World(){};
    
    void createWorld(); //function for initialization
    void simulate();
    void report(ReportParameters& report_h, ReportMosquito& report_f, ReportLarva& report_l); //-> don't think I need it after all...maybe as a logger in a later stage
    
    bool isItInfectionTime();
    void introduceParasite(const string _path);
    //------------
    void removeDeadIndividuals(); //ok
    void addNewIndividuals(); //works
    
    vector<Larva> larvae;
    vector<Female> femaleMosquitoes;
    vector<Human> humans;
    vector<Parasite> parasites;
    
    
    
protected:
    
    double simulationTime;
    double timeStep;
    double timeEnd;
    
    unsigned long int id_counter_mosquitoes;
    unsigned long int id_counter_humans;
    
};


#endif /* World_hpp */
