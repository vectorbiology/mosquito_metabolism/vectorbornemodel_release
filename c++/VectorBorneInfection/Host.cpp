//
//  Human.cpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#include "Host.hpp"

Larva :: Larva(double _simTime)
{
    Individual :: initializeParameters();
    adult = false;
    
    double temperature =Parameters::temperature;
    if(Parameters::seasonality)
    {
        temperature = Parameters::temperatureProfile.at(int(_simTime)% 730);
    }
    
    deathRate = calculateLarvaDeathRate(temperature, Parameters::a_d, Parameters:: eta_a_d, Parameters::b_d, Parameters:: eta_b_d, Parameters::c_d, Parameters:: eta_c_d);
    
    growthRate = calculateIndividualEffects(Parameters::larvaGrowthRate, Parameters::eta_larvaGrowthRate);
    
    timePupation = calculateTimePupation(temperature, Parameters::m_t, Parameters::eta_m_t, Parameters::k_t, Parameters::eta_k_t, Parameters::c_t, Parameters::eta_c_t);

}

Human :: Human()
{
    Individual :: initializeParameters();
    reproducing = false;
    infectionState = susceptible;
    recoveryRate = 0.0;
    parasiteLoad = 0.0;
    dailyBites = 0;
}

//perform a deep copy of a human
Human& Human :: Copy(Human& rhs)
{
    if(this == &rhs)
        return *this;
    
    //copying member variables
    this->Individual::Copy(rhs);
    
    this->reproducing = rhs.reproducing;
    this->infectionState = rhs.infectionState;
    this->recoveryRate = rhs.recoveryRate;
    this->parasiteLoad = rhs.parasiteLoad;
    this->dailyBites = rhs.dailyBites;
    
    return *this;
}

void Human::clearInfection()
{
    //human will clear the infection, the parasite should be reset
  /*  this->falciperum.resetParasite();
    this->parasiteLoad = 0.0; //reset again the parasiteLoad ...-> this doesn't seem like the best design...i am changing two things in two different spots which are actually the same... maybe use pointers? don't like it!
    this->setInfectionState("recovered");*/
    
    ///*
    if(randomNumberDouble()<Parameters::reservoirProbability)
    {
        //the human will keep the parasite but with less virulence
        this->setInfectionState("reservoir");
        this->parasiteLoad = Parameters::parasitemiaReservoir;
    }
    else
    {
        //human will clear the infection, the parasite should be reset
        this->falciperum.resetParasite();
        this->parasiteLoad = 0.0; //reset again the parasiteLoad ...-> this doesn't seem like the best design...i am changing two things in two different spots which are actually the same... maybe use pointers? don't like it!
        this->setInfectionState("recovered");
    }//*/
}

void Human::infectHuman(Female *mosquito)
{
    this->falciperum.Copy(mosquito->getParasite());
}

Female :: Female()
{
    Individual :: initializeParameters();
    numberOfEggs = 0;
    ovipositionWaitingTime = 0.0;
    mosquitoStage = seeking;
    infectionState = susceptible;
    transmissionType = uninfected;
    numberHumanInfections = 0;
    numberBloodMeals = 0;
    eggEnergy = 1.0;
    h2m = false;
    m2h = false;
}

void Female :: resetTransmissionFlags()
{
    h2m=false;
    m2h = false;
}

void Female::setMosquitoStage(const string name)
{
    
    if(name.compare("seeking")==0)
        this->mosquitoStage=seeking;
    else if(name.compare("biting")==0)
        this->mosquitoStage=biting;
    else if(name.compare("layingEggs")==0)
        this->mosquitoStage=layingEggs;
    else
    {
        cout <<"attempt to assign wrong state to the mosquito!\n";
        exit(9);
    }
}

void Female::setMosquitoTransmissionType(const string name)
{
    
    if(name.compare("uninfected")==0)
        this->transmissionType=uninfected;
    else if(name.compare("carrier")==0)
        this->transmissionType=carrier;
    else if(name.compare("spreader")==0)
        this->transmissionType=spreader;
    else if(name.compare("superSpreader")==0)
        this->transmissionType=superSpreader;
    else
    {
        cout <<"attempt to assign wrong transmission state to the mosquito!\n";
        exit(9);
    }
}

void Female :: setSpreaderStatus()
{
    if(numberHumanInfections ==0 && this->transmissionType == uninfected)
    {
        this->transmissionType =carrier;
        return;
    }
    
    if (numberHumanInfections == 0 && this->transmissionType == carrier)
    {
        this->transmissionType=spreader;
        numberHumanInfections ++;
        return;
    }
    if(numberHumanInfections >= 1 && this->transmissionType == spreader)
    {
        this->transmissionType=superSpreader;
        numberHumanInfections ++;
        return;
    }
}
