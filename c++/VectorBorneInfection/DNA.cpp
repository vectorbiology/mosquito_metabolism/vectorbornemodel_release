//
//  DNA.cpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#include "DNA.hpp"

ParasiteDNA::ParasiteDNA()
{
    id = 0.0;
}

//perform a deep copy of the dna
ParasiteDNA& ParasiteDNA :: Copy(ParasiteDNA& rhsDNA)
{
    if(this == &rhsDNA)
        return *this;
    
    //copying member variables
    this->id = rhsDNA.id;
    
    return *this;
}
