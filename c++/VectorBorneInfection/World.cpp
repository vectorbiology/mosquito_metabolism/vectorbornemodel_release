//
//  World.cpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#include "World.hpp"
#include "Parameters.hpp"


World :: World()
{
    simulationTime = 0.0;
    
    
}

void World :: createWorld()
{
    
    for(int i = 0; i<Parameters::humanPopulationSize; i++)
    {
        Human dummy;
        int age =randomNumber(1, 90);
        dummy.setAge(age);
        dummy.setID(i);
        //dummy.setRecoveryRate(calculateAgedependentRecoveryRate(age, Parameters::maxRecoveryRate));
        dummy.setRecoveryRate(Parameters::maxRecoveryRate);
        humans.push_back(dummy);
    }
    
    
    for(int i = 0; i<Parameters::mosquitoPopulationSize; i++)
    {
        Female mosquita;
        mosquita.setAge(randomNumber(0, 10));
        mosquita.setID(i);
        femaleMosquitoes.push_back(mosquita);
    }
    
    for(int i = Parameters::mosquitoPopulationSize; i< Parameters::larvaPopulationSize+Parameters::mosquitoPopulationSize; i++)
    {
        Larva larvita(0.0);
        larvita.setAge(randomNumber(0,10));
        larvita.setID(i);
        larvae.push_back(larvita);
    }
    
    // initialize individuals' id
    id_counter_mosquitoes = larvae.size() + femaleMosquitoes.size();
    id_counter_humans = humans.size();
}

void World :: simulate()
{
    
    //initialize the population sizes
    Parameters::humanPopulationSize = humans.size();
    Parameters::larvaPopulationSize = larvae.size();

    //set output path
    Output dir;
    dir.createResultsDirectory(Parameters::outDir);
    const string path = dir.getPath();
    
    //open the population file
    ofstream populationSize;
    const string populationFile(path + "PopulationSize.csv");
    populationSize.open(populationFile.c_str());
    
    populationSize << "Time;H;L;M;humanBirths;mosquitoBirths;humanDeaths;mosquitoDeaths;larvaDeaths;humanInfections;mosquitoInfections\n";
    
    //open the temperature file
    ofstream temperatureProfile;
    const string temperetureFile(path + "TemperatureProfile.csv");
    temperatureProfile.open(temperetureFile.c_str());
    
    temperatureProfile << "Time;Temperature\n";
    
    //open the spreader file
    ofstream spreaderMosquito;
    const string spreaderMosquitoFile (path + "TransmissionProfileMosquitoes.csv");
    spreaderMosquito.open(spreaderMosquitoFile.c_str());
    
    spreaderMosquito << "Time;Uninfected;Carrier;Spreaders;SuperSpreaders\n";
    
    //Define the Reporters
    
    ReportParameters report_h(path + "humanParameters.csv");
    ReportMosquito report_f(path + "femaleParameters.csv");
    ReportLarva report_l(path + "larvaParameters.csv");
    
    
    while (simulationTime < Parameters::timeEnd)
    {
        
        // Define the Events
        Death deathEvent;
        Birth birthEvent;
        Grow growthEvent(simulationTime);
      
        Bite bitingEvent(simulationTime);
        UpdateParameters updateEvent(simulationTime);
        
        Visitor worldVisitor;
        
        // intoduce the pathogen
        introduceParasite(path);
        
  //*
        random_shuffle(humans.begin(), humans.end());
        vector<Human>::iterator it_human;
        
        for(it_human = humans.begin(); it_human!= humans.end(); it_human++)
        {
            
           // worldVisitor.setEvent(&birthEvent);
           // it_human->accept(&worldVisitor);
            
            worldVisitor.setEvent(&deathEvent);
            it_human->accept(&worldVisitor);
            
            worldVisitor.setEvent(&report_h);
            it_human->accept(&worldVisitor);
            
            worldVisitor.setEvent(&updateEvent);
            it_human->accept(&worldVisitor);

        }

        random_shuffle(larvae.begin(), larvae.end());
        vector<Larva>::iterator it_larva;
        
        for(it_larva=larvae.begin(); it_larva<larvae.end(); it_larva++)
        {
            
            worldVisitor.setEvent(&growthEvent);
            it_larva->accept(&worldVisitor);
            
            worldVisitor.setEvent(&deathEvent);
            it_larva->accept(&worldVisitor);
            
            worldVisitor.setEvent(&report_l);
            it_larva->accept(&worldVisitor);
            
            worldVisitor.setEvent(&updateEvent);
            it_larva->accept(&worldVisitor);

        }//*/

        random_shuffle(femaleMosquitoes.begin(), femaleMosquitoes.end());
        vector<Female>::iterator it_mosquito;
        
        for(it_mosquito = femaleMosquitoes.begin(); it_mosquito !=femaleMosquitoes.end(); it_mosquito++)
        {
        
            if(humans.size()>0) //biting event should only occur if there are any humans to bite
            {

                bitingEvent.pickHuman(&humans.at(randomNumber(0,humans.size()-1)));
                worldVisitor.setEvent(&bitingEvent);
                it_mosquito->accept(&worldVisitor);
            }
            else{
                cout<< "human population is zero.... mosquitos should die soon!\n";
            }
            
            worldVisitor.setEvent(&birthEvent);
            it_mosquito->accept(&worldVisitor);
            
            worldVisitor.setEvent(&deathEvent);
            it_mosquito->accept(&worldVisitor);
            
            worldVisitor.setEvent(&report_f);
            it_mosquito->accept(&worldVisitor);
            
            worldVisitor.setEvent(&updateEvent);
            it_mosquito->accept(&worldVisitor);

        }
        
        removeDeadIndividuals();
        addNewIndividuals();
     
        //save the population Sizes
        populationSize << simulationTime << ";" << humans.size() << ";"<<larvae.size() <<  ";"<<femaleMosquitoes.size()<<";";
        birthEvent.reportBirthNumbers(populationSize);
        deathEvent.reportDeathNumbers(populationSize);
        bitingEvent.reportInfectionNumbers(populationSize);
        populationSize<<"\n";
        
        //save the temperature Profile
        temperatureProfile << simulationTime << ";" << Parameters::temperatureProfile.at(int(simulationTime)% 730)<<"\n";
        //cout << "time: " << simulationTime <<endl;
        
        //save the mosquito transmission profile
        bitingEvent.reportSpreaderNumbers(spreaderMosquito, simulationTime);
        
        cout << "time:  "<<simulationTime << "\t" << humans.size() << "\t"<<larvae.size() <<  "\t"<<femaleMosquitoes.size()<<endl;
        
        //update the population sizes
        Parameters::humanPopulationSize = humans.size();
        Parameters::larvaPopulationSize = larvae.size();
        
        simulationTime = simulationTime + Parameters::timeStep;
        
        //update the time for the reporters:
        report_h.setSimulationTime(simulationTime);
        report_f.setSimulationTime(simulationTime);
        report_l.setSimulationTime(simulationTime);
        
        //update the time for the growth event
        growthEvent.setSimulationTime(simulationTime);
    }
    populationSize.close();
    temperatureProfile.close();
}




//------------------------------------------------------------------------------------------------------------------------------------
void World::removeDeadIndividuals()
{
    //	remove Humans
    vector<Human>::iterator it_h = humans.begin();
    
    while(it_h!=humans.end())
    {
        if(it_h->isDead())
        {
            //replace dead individuals by a new susceptible one
            Human baby;
            baby.setRecoveryRate(Parameters::maxRecoveryRate);
            baby.setAge(randomNumber(1, 90));
            baby.setID(id_counter_humans);
            it_h->Copy(baby);
            //it_h=humans.erase(it_h);
            //humans.push_back(baby);
            id_counter_humans ++;
            
            //continue;
        }
        it_h++;
    }
   
    //remove adult mosquitoes
    vector<Female>::iterator it = femaleMosquitoes.begin();
    
    while(it!=femaleMosquitoes.end())
    {
        if(it->isDead())
        {
            it = femaleMosquitoes.erase(it);
            continue;
        }
        it++;
    }
    
    //remove larvae
    vector<Larva>::iterator it_l = larvae.begin();
    
    while(it_l!=larvae.end())
    {
        if(it_l->isDead())
        {
            it_l = larvae.erase(it_l);
            continue;
        }
        it_l++;
    }
}

void World::addNewIndividuals()
{
    //	add newBorn Humans
  /*  vector<Human>::iterator it_h = humans.begin();
    vector<Human> ::iterator it_h_end = humans.end();
    
    while(it_h!=it_h_end)
    {
        if(it_h->isReproducing())
        {
            Human baby;
            baby.setID(id_counter_humans);
            humans.push_back(baby);
            id_counter_humans ++;
            
            it_h->setReproducing(false);
        }
        it_h++;
    }*/
 
    //add layed eggs
    vector<Female>::iterator it = femaleMosquitoes.begin();
    vector<Female>::iterator it_end = femaleMosquitoes.end();
    
    while(it!=it_end)
    {
        for(int i=0; i< it->getNumberEggs(); i++)
        {
            Larva larvita(simulationTime);
            larvita.setID(id_counter_mosquitoes);
            larvae.push_back(larvita);
            id_counter_mosquitoes ++;
        }
        it->setNumberEggs(0); //after oviposition the female is empty
        it++;
    }
    
    //add new Adults
    vector<Larva>::iterator it_l = larvae.begin();
    //vector<Larva>::iterator it_l_end = larvae.end();
    
    while(it_l != larvae.end())//it_l_end)
    {
        if(it_l->isAdult())
        {
            Female mosquita;
            mosquita.setID(it_l->getID());
            femaleMosquitoes.push_back(mosquita);
            
            it_l = larvae.erase(it_l);
            continue;
        }
        it_l++;
    }
    
}

bool World::isItInfectionTime()
{
    if(floor(Parameters::timeToStart -simulationTime)>0 && floor(Parameters::timeToStart-simulationTime)<2.0)
        return true;
    else
        return false;
}

void World::introduceParasite(const string path)
{
    if(isItInfectionTime())
    {
        //save incubation period profile
        
        ofstream mosquitoIncubation;
        const string mosquitoIncubationFile (path + "IncubationPeriod.csv");
        mosquitoIncubation.open(mosquitoIncubationFile.c_str());
        
        mosquitoIncubation << "ID;IncubationPeriod\n";
        
        Parasite malaria;
        malaria.setHumanVirulence(Parameters::parasitemia);
        
        // set the initial probability of transmission of the metabolism simulations
        if(Parameters::metabolism)
        {
            malaria.setInitialTransmissionProb(0.2);
        }
        
        for(unsigned int i = 0; i<0.20*humans.size() ; i++)
        {

            double incubation = Parameters::incubationPeriodMosquitoes.at(randomNumber(0, Parameters::incubationPeriodMosquitoes.size()-1));
            //double incubation = Parameters::meanParasiteDevelopmentInMosquito;
            malaria.setIncubationPeriodMosquito(incubation);
            //malaria.getDNA().setDNAID(i+1);
            
            humans.at(i).getParasite().Copy(malaria);
            
            if(Parameters::initialInfection)
            {
                humans.at(i).setParasiteLoad(Parameters::parasitemiaReservoir);
                humans.at(i).setInfectionState("reservoir");
            }
            else
            {
                humans.at(i).setParasiteLoad(malaria.getHumanVirulence());
                humans.at(i).setExposureTime(simulationTime);
                humans.at(i).setInfectionState("exposed");
            }
            
            mosquitoIncubation << i+1<<";"<<incubation <<"\n";
        }
        mosquitoIncubation.close();
    }//*/
    
    
}
