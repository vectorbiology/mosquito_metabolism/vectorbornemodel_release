/*
 * MathFunctions.cpp
 *
 *  Created on: Apr 19, 2011
 *      Author: paola
 */

#include "MathFunctions.h"

//Prepare the randomizer

namespace MathFunctions{
   boost::mt19937 boostRandomSeed(static_cast<unsigned int>(time(NULL)));
  // boost::mt19937 boostRandomSeed(0); // fixing seed for demo
   boost::uniform_real<> u01( 0., 1. );
   
   boost::variate_generator<boost::mt19937&, boost::uniform_real<> > s01(boostRandomSeed, u01);
   boost::variate_generator<boost::mt19937&, boost::uniform_real<> > randomNumberDouble(boostRandomSeed, u01);
   
}


int randomNumberSmallInt(int min, int max)
{
	boost::uniform_smallint<> dist(min,max);
	boost::variate_generator<boost::mt19937&, boost::uniform_smallint<> > die(MathFunctions::boostRandomSeed, dist);
	return die();
}

int randomNumber(int min, int max)
{
   return (int)(MathFunctions::s01()*(max-min+1))+min;
	//boost::uniform_int<> dist(min,max);
	//boost::variate_generator<boost::mt19937&, boost::uniform_int<> > die(boostRandomSeed, dist);
	//return die();
}
double randomNumberDouble(int min, int max)
{
   double r = MathFunctions::s01();
   return r*(max-min)+min;
}

double randomNumberDouble()
{
   return MathFunctions::s01();
}

double normalDistribution(double mean, double sd)
{
    boost::normal_distribution<> nd(mean, pow(sd,2.0));
    boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > var_nor(MathFunctions::boostRandomSeed, nd);
    return var_nor();
}

// ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- -------------

int numberEggs(double mean, double sigma)
{
    int numberEggs = normalDistribution(mean, sigma);
    while(numberEggs<0)
    {
        numberEggs = normalDistribution(mean, sigma);
    }
    
    return(numberEggs);
}



double calculateMean(vector<int>& values)
{
    double sum = std::accumulate(values.begin(), values.end(), 0.0);
    double mean = sum / values.size();
    return mean;
}

double calculateStandardDeviation(vector<int>& values)
{

    double _mean = calculateMean(values);
    double sq_sum = std::inner_product(values.begin(), values.end(), values.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / values.size() - _mean * _mean);
    
    return stdev;

}

double getAgeDependentRate(vector<double> _rateVector, int age)
{
    return _rateVector.at(age);
}

vector<double> readFile(const string _deathRatesFile)
{
    
    ifstream deathRatesFile;
    vector<double> testVector;
    string line;
    
    if(_deathRatesFile.compare("please define a file") == 0)
    {
        cerr << "you forgot to specify a file in the parameter file!\n Aborting...\n";
        exit(0);
    }
    //if the name equals "please define a file" then return error saying you probably forgot to give a parameter file
    
    deathRatesFile.open(_deathRatesFile);
    if (!deathRatesFile.is_open())
    {
        cerr << "Can't open " << _deathRatesFile << "!\n Aborting...\n";
        exit(0);
    }else
    {
        while(getline(deathRatesFile,line))
        {
            istringstream ss(line);
            double n;
            
            while(ss >> n)
            {
                testVector.push_back(n);
            }
        }
    }
    deathRatesFile.close();
    return testVector;
}

double getDensityDependentDeathRate(double _deathRate, double populationSize, double K)
{
    double d = _deathRate*(1+ populationSize/(K*0.99753));
    return d;
}

double getDensityDependentBirthRate(double _birthRate, double populationSize, double K)
{
    double b = _birthRate*(1-(populationSize/(K*0.99753)));
    return b;
}

double calculateGompertzHazard(double shape, double rate, double age)
{
    double hazard = rate * exp(shape*age);
    return hazard;
}

/*double calculateTemperatureDependentGompertzHazard(double T, double age)
{
    double a0 = 119.193;
    double a1 = -8.31;
    double a2 = 0.148;
    
    double b0 = 95.622;
    double b1 = -2.7208;
    
    //this function calculates the temperature dependent hazard, according to the gompertz survival distribution
    double a = a0 + a1*T + a2*T*T;
    double b = b0 + b1*T;
    
    double hazard = calculateGompertzHazard(b, a);
    return hazard;
}*/

vector<double> createMosquitoDeathRates(double shape, double rate)
{
    vector<double> deathRates;
    try
    {
        for(unsigned long int j = 0; j <= 60; j++)
        {
            double i = j/1.0;
            double death = calculateGompertzHazard(shape,rate,i);
            deathRates.push_back(death);
        }
    }
    catch(...){
        cout << "caught something in the mosquito death rates" <<endl;
    }
    return deathRates;
}



vector<double> createHumanDeathRates()
{
    vector<double> deathRates;
    try
    {
        for(unsigned long int j=0; j <105; j++)
        {
            double i = j/1.0;
            double death = exp(0.1*i-10.5)+ exp(-0.4*i-8);
            deathRates.push_back(death);
            //cout << i << " "<< death << endl;
        }
    }
    catch (...) {
        cout << "caught something" << endl;
    }
    return deathRates;
}

vector<double> createHumanBirthRates()
{
    vector<double> birthRates;
    try
    {
        for(unsigned long int j=0; j <105; j++)
        {
            double i = j/1.0;
            double birth =  1/(1+exp(i-45)) -1/(1+ exp(i-20));
            birthRates.push_back(birth);
            //cout << i << " "<< birth << endl;
        }
    }
    catch (...) {
        cout << "caught something" << endl;
    }
    return birthRates;
}

double getLarvaGrowthRate(int age, double growthRate,double larvaPopulationSize, double K, double timePupation)
{
    double rate;
    if(age <=timePupation) 
        rate = 0;
    else
        rate = growthRate*exp(-log(2)*larvaPopulationSize/K);
    
    return rate;
}



double calculateIndividualEffects(double mean, double eta)
{
    double value = mean * exp(normalDistribution(0.0, eta));
    return value;
}


double convertRate2Probability(double _rate)
{
    double probability = 0.0;
    
    probability = 1 - exp(- _rate);
    return probability;
}


double calculateBitingRate(double prob, int N, int h)
{
    double rate = prob*(pow(N,3)/(pow(N,3)+pow(h,3)));
    return rate;
}

double calculateAgedependentRecoveryRate(double age, double maxRate)
{
    double rate = maxRate*( pow(age,2) / (pow(age,2) + 49));
    return rate;
}

vector<double> createIncubationPeriod(double _mean, double _sd)
{
    vector<double> result;
    
    for(int i = 0; i<=1000; i++)
    {
        double a =normalDistribution(_mean, _sd);
        while(a <=0)
            a =normalDistribution(_mean, _sd);
        
        result.push_back(a);
    }
    return result;
}
double calculateLarvaDeathRate(double _temp, double _a, double eta_a, double _b, double eta_b, double _c, double eta_c)
{
    
    
    // substraction of 28 according to the definition of the ODE model from Juan
    double temp = _temp - 28.0;
    double a = calculateIndividualEffects(_a, eta_a);
    double b = calculateIndividualEffects(_b, eta_b);
    double c = calculateIndividualEffects(_c, eta_c);
    double deathRate = (a*pow(temp, 2.0) - b*temp + c);
        
    while(deathRate <0)
    {
        a = calculateIndividualEffects(_a, eta_a);
        b = calculateIndividualEffects(_b, eta_b);
        c = calculateIndividualEffects(_c, eta_c);
        deathRate = (a*pow(temp, 2.0) - b*temp + c);
    }
    return deathRate;

}

double calculateTimePupation(double _temp, double _m, double eta_m, double _k,double eta_k, double _c, double eta_c)
{
    
    // substraction of 28 according to the definition of the ODE model from Juan
    double temp = _temp - 28.0;
    double m = calculateIndividualEffects(_m, eta_m);
    double k = calculateIndividualEffects(_k, eta_k);
    double c = calculateIndividualEffects(_c, eta_c);
    
    
    double _timePupation = m/(1+exp(k*temp)) + c;
    return _timePupation;

}
