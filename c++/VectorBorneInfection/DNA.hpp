//
//  DNA.hpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#ifndef DNA_hpp
#define DNA_hpp

#include <stdio.h>
#include "MathFunctions.h"
#include "Parameters.hpp"

class DNA
{
public:
    DNA(){};
    virtual ~DNA(){};
};



class MosquitoDNA : public DNA
{
public:
    MosquitoDNA(){};
    virtual ~MosquitoDNA(){};
};



class HumanDNA : public DNA
{
public:
    HumanDNA(){};
    virtual ~HumanDNA(){};
    
};


class ParasiteDNA : public DNA
{
public:
    ParasiteDNA();
    virtual ~ParasiteDNA(){};
    
    double getDNAID(){return id;}
    void setDNAID(double id){this->id=id;}
    
    ParasiteDNA& Copy(ParasiteDNA& rhsParasite);
protected:
    double id;
};


#endif /* DNA_hpp */
