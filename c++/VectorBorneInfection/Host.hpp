//
//  Host.hpp ->specific Nodes of class Individual


//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#ifndef Host_hpp
#define Host_hpp

#include <stdio.h>
#include "Visitor.hpp"
#include "Parasite.hpp"
#include "Parameters.hpp"

class Human : public Individual
{
public:
    Human();
    virtual ~Human(){};
    
    virtual void accept(Visitor *v){v->visit(this);};
    
    bool isReproducing(){return reproducing;};
    void setReproducing(bool _reproducing){this->reproducing = _reproducing;};
    
    void infectHuman(Female *mosquito);
    void clearInfection();
    double getRecoveryRate(){return recoveryRate;};
    void setRecoveryRate(double _rate){this->recoveryRate = _rate;};
    
    double getParasiteLoad(){return this->parasiteLoad;};
    void setParasiteLoad(double _load){this->parasiteLoad = _load;};
    Parasite& getParasite(){return this->falciperum;}
    
    void increaseDailyNumberOfBites(){dailyBites ++;}
    int getNumberOfBites(){return this->dailyBites;}
    void resetNumberBites(){this->dailyBites = 0;}
    
    Human& Copy(Human& rhsHuman);

protected:
    HumanDNA genotype;
    bool reproducing;
    double recoveryRate;

    
    Parasite falciperum;
    double parasiteLoad;
    int dailyBites;
    //double attractiveness;
};


class Mosquito : public Individual
{
public:
    Mosquito(){};
    virtual ~Mosquito(){};
    
    virtual void accept(Visitor* v)=0;
    
    
protected:
    MosquitoDNA genotype;
    
};


class Larva : public Mosquito
{
public:
    Larva(double _simTime); //default constructor
    virtual ~Larva(){};
    
    virtual void accept(Visitor* v){v->visit(this);};
    
    void grow();
    bool isAdult(){return adult;};
    void setAdult(){adult = true;};
    
    double getDeathRate(){return deathRate;};
    double getGrowthRate(){return growthRate;};
    double getTimePupation(){return timePupation;};
    
protected:
    bool adult;
    double deathRate;
    double growthRate;
    double timePupation;
    
    
};


class Female : public Mosquito
{
public:
    Female();
    virtual ~Female(){};
    
    virtual void accept(Visitor* v){v->visit(this);};
    
    int getNumberEggs(){return numberOfEggs;};
    void setNumberEggs(int _n){numberOfEggs = _n;};
    
    double getWaitingTime(){return this->ovipositionWaitingTime;};
    void setWaitingTime(double _time){this->ovipositionWaitingTime = _time;}

    
    enum state{seeking, biting, layingEggs};
    void setMosquitoStage(const string name);
    
    bool isMosquitoSeeking(){return(mosquitoStage == seeking);};
    bool isMosquitoLayingEggs(){return(mosquitoStage == layingEggs);};
    bool isMosquitoBiting(){return(mosquitoStage == biting);};
    
    Parasite& getParasite(){return this->falciperum;}
    
    void setHuman2MosquitoTransmission(bool _h2m){this->h2m = _h2m;};
    void setMosquito2HumanTransmission(bool _m2h){this->m2h = _m2h;};
    
    bool isH2MTrue(){return h2m;};
    bool isM2HTrue(){return m2h;};
    void resetTransmissionFlags();
    
    enum type{uninfected, carrier, spreader, superSpreader};
    void setMosquitoTransmissionType(const string name);
    
    bool isMosquitoUninfected(){return(transmissionType == uninfected);};
    bool isMosquitoCarrier(){return(transmissionType == carrier);};
    bool isMosquitoSpreader(){return(transmissionType == spreader);};
    bool isMosquitosSuperSpreader(){return(transmissionType == superSpreader);};
    int getNumberHumanInfections(){return this->numberHumanInfections;};
    int getNumberBloodMeals(){return this->numberBloodMeals;};
    void setNumberBloodMeals(int x){this->numberBloodMeals = x;}
    void setSpreaderStatus();
    double getEggEnergy(){return this->eggEnergy;};
    void setEggEnergy(double _energy){this->eggEnergy=_energy;};
    
protected:
    
    int numberOfEggs;
    double ovipositionWaitingTime; //waiting time between a blood meal and the actual oviposition

    state mosquitoStage;
    Parasite falciperum;
    type transmissionType;
    
    int numberHumanInfections;
    int numberBloodMeals;
    double eggEnergy;
    bool h2m; //human->mosquito transmission
    bool m2h; //mosquito->human transmission
};


class Male : public Mosquito
{
public:
    Male(){};
    virtual ~Male(){};
    
    virtual void accept(Visitor* v){v->visit(this);};
};


#endif /* Host_hpp */
