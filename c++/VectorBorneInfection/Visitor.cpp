//
//  Visitor.cpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#include "Visitor.hpp"
#include "Host.hpp"



// Death-event class ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Death::Death()
{
    humanDeaths = mosquitoDeaths = larvaDeaths =0;
}

void Death::execute(Human* v)
{
   // double rate = getAgeDependentRate(Parameters::ageHumanDeathRates, int(v->getAge())) * Parameters::humanDeathRate + v->getParasiteLoad();
    
    double rate = Parameters::humanDeathRate+v->getParasiteLoad();
    if(randomNumberDouble()<rate)
    {
        v->setDead();
        humanDeaths ++;
    }
}


void Death::execute(Larva* v)
{
    //double rate = Parameters::larvaDeathRate;
   
    // Temperature-dependent death probability
    double probability = convertRate2Probability(v->getDeathRate());
    if(randomNumberDouble()<probability)
    {
        v->setDead();
        larvaDeaths++;
    }
}

void Death::execute(Female* v)
{
    double rate = Parameters::mosquitoDeathRate* getAgeDependentRate(Parameters::ageMosquitoDeathRates, v->getAge());
    if(v->isExposed() | v->isInfected())
        rate = rate*1.08;
    
    if(randomNumberDouble()<rate)
    {
        v->setDead();
        mosquitoDeaths ++;
    }
}

void Death::reportDeathNumbers(ofstream& file)
{
    file << humanDeaths <<";"<<mosquitoDeaths<<";"<<larvaDeaths<<";";
}

void Death::getDeathNumbers()
{
    cout <<humanDeaths <<"\t"<<mosquitoDeaths<<"\t"<<larvaDeaths<<"\n";
}

// Birth event class ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Birth::Birth()
{
    humanBirths = mosquitoBirths = 0;
}


void Birth::execute(Human * v)
{
    double ageDependent =getAgeDependentRate(Parameters::ageHumanBirthRates, int(v->getAge()));
    double densityDependent = getDensityDependentBirthRate(Parameters::humanBirthRate, Parameters::humanPopulationSize, Parameters::K);
    double rate =  ageDependent * densityDependent; //maybe here i should use the product rules of probabilities
    
    if(randomNumberDouble()<rate)
    {
        v->setReproducing(true);
        humanBirths ++;
   
    }
}

void Birth::execute(Female* v)
{
    if(v->isMosquitoLayingEggs())
    {
        double rate = getDensityDependentBirthRate(Parameters::mosquitoBirthRate, Parameters::larvaPopulationSize, Parameters::BIOMASS);
        if(randomNumberDouble()<rate)
        {
            v->setNumberEggs(numberEggs(MEANEGGS, SDEGGS)*v->getEggEnergy());
            mosquitoBirths = mosquitoBirths + v->getNumberEggs();
        }
        v->setMosquitoStage("seeking");
    }
}

void Birth::reportBirthNumbers(ofstream& file)
{
    file << humanBirths <<";"<<mosquitoBirths<<";";
}

void Birth::getBirthNumbers()
{
    cout <<humanBirths <<"\t"<<mosquitoBirths<<"\n";
}

// Grow event class ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Grow::execute(Larva* v)
{
 
    double maxProb = 0.0;
    
    //Temp-dependent growth rate --> it should be computed here, right? 
    // get density dependent growth rate
    //double rate =  getLarvaGrowthRate(v->getAge(),Parameters::larvaGrowthRate,Parameters::larvaPopulationSize, Parameters::BIOMASS);
    
    double rate =  getLarvaGrowthRate(v->getAge(),v->getGrowthRate(),Parameters::larvaPopulationSize, Parameters::BIOMASS, v->getTimePupation());
    if(Parameters::seasonality)
    {
        // multiply this rate with the temperature profile
        int _time = 0;
        
        if(floor(simTime - 365.0) >=0) //365.0 is because the cycle should repeat after one year and we are using oe day as a time step
            _time = int(simTime) % 365; //... note that this would only work if I have days as a timestep, as the modulo operator works only with ingtegers
        else
            _time = int(simTime);
        
        //maxProb = rate*Parameters::temperatureDependentGrowthRates.at(_time);
        maxProb = convertRate2Probability(rate*Parameters::temperatureProfile.at(_time));
        // cout << simTime << "\t" << _time << "\t" <<Parameters::temperatureDependentGrowthRates.at(_time) << "\t" << rate <<endl;
        
    }
    else
        maxProb = convertRate2Probability(rate);

    if(randomNumberDouble()<maxProb)
        v->setAdult();
}

// Bite and Trasmit event event class ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Bite::Bite(double _simTime)
{
    this->simTime = _simTime;
    humanInfections = mosquitoInfections = 0;
    uninfectedMosquitoes = carrierMosquitoes = spreadersMosquitoes = superSpreadersMosquitoes = 0;
}

void Bite::countSpreaders(Female * v)
{
    if(v->isMosquitoUninfected())
        uninfectedMosquitoes++;
    else if(v->isMosquitoCarrier())
        carrierMosquitoes++;
    else if(v->isMosquitoSpreader())
        spreadersMosquitoes++;
    else if(v->isMosquitosSuperSpreader())
        superSpreadersMosquitoes++;
    else
    {
        cout <<"Mosquito transmission status not found! there must be something seriously wrong\n";
        exit(9);
    }
}

void Bite::pickHuman(Human * _human)
{
    this->human = _human;
}


void Bite::execute(Female* v)
{
    double densityDependentBitingRate = 0.5 * calculateBitingRate(Parameters::bitingRate, Parameters::humanPopulationSize, Parameters::K/3); //0.5 because only females bite
    if(simTime>0.0 && v->isMosquitoSeeking()) //the first day of the simulation we do not allow any female to bite
    {
        if(randomNumberDouble()<densityDependentBitingRate)
        {
            v->setMosquitoStage("biting");
            v->setNumberBloodMeals(v->getNumberBloodMeals()+1);
            v->setWaitingTime(simTime);
            
            double newTransmissionProb = Parameters::transmissionProbability;
            if(Parameters::metabolism)
            {
                newTransmissionProb = v->getParasite().getParasiteValue();
            }
            
           // cout<< newTransmissionProb <<endl;
            transmitParasite(v, newTransmissionProb);
            this->human->increaseDailyNumberOfBites();
        }
    }
    countSpreaders(v);

}

void Bite::transmitParasite(Female* mosquito, double newTransmissionProb)
{

    if(human->isSusceptible() && mosquito->isInfected())
    {
        //copy mosquito parasite into human!
        //human->infectHuman(mosquito);
        
        if(randomNumberDouble()<newTransmissionProb) //mosquito to human transmssion is affected by metabolism
        {
            human->getParasite().Copy(mosquito->getParasite());
            human->getParasite().setParasiteBM(0); //reset the number of blood meals to 0
            human->getParasite().setParasiteValue(0.0);//reset the energy to 0.0
            human->setInfectionState("exposed");
            mosquito->setSpreaderStatus(); //this functions checks whether this is the first infection or the second one and keeps track of the number of infected humans
            human->setExposureTime(simTime);
            humanInfections ++;
            
            //Parasite mutates its EIR here:
            if(simTime >=Parameters::timeOfMutation)
            {
                if(randomNumberDouble()< Parameters::parasiteMutationRate)
                {
                    if(mosquito->getParasite().getIncubationPeriodMosquito()>=5.0) //setting the minimal EIP value allowed
                        mosquito->getParasite().mutateEIP();
                }
            }

            mosquito->setMosquito2HumanTransmission(true);
        }

    }
    
    if(human->isInfected() && mosquito->isSusceptible())
    {
        if(human->getParasite().getParasiteBM()>0.0 | human->getParasite().getParasiteValue()>0.0)
        {
            cout<<"error, parasite should not have a bm in the human or a high energy value!!! \t";
            cout <<human->getParasite().getParasiteBM()<< "|"<< human->getParasite().getParasiteValue() <<endl;
            exit(-9);
        }
        if(randomNumberDouble()<Parameters::transmissionProbability) //human to mosquito transmisison probability should be constant and not affected by mosquito metabolism
        {
            mosquito->getParasite().Copy(human->getParasite());
            mosquito->setInfectionState("exposed");
            mosquito->setSpreaderStatus(); //this functions checks whether this is the first infection or the second one and keeps track of the number of infected humans
            mosquito->setExposureTime(simTime);
            
            mosquitoInfections++;
            
            //Parasite mutates its EIR here:
            if(simTime >=Parameters::timeOfMutation)
            {
                if(randomNumberDouble()< Parameters::parasiteMutationRate )
                {
                    if(mosquito->getParasite().getIncubationPeriodMosquito()>=5.0)  //setting the minimal EIP value allowed
                        mosquito->getParasite().mutateEIP();
                }
            }
            mosquito->setHuman2MosquitoTransmission(true);
        }
    }
    
    if(human->isReservoir() && mosquito->isSusceptible())
    {
   
        if(human->getParasite().getParasiteBM()>0)
        {
            cout<<"error, parasite should not have a bm in the human\t";
            cout <<human->getParasite().getParasiteBM()<<endl;
            exit(-9);
        }
        
        if(randomNumberDouble() < 0.1*Parameters::transmissionProbability) //transmission from reservoir should be less than normal transmission
        {
            mosquito->getParasite().Copy(human->getParasite());
            mosquito->setInfectionState("exposed");
            mosquito->setSpreaderStatus(); //this functions checks whether this is the first infection or the second one and keeps track of the number of infected humans
            mosquito->setExposureTime(simTime);
            
            mosquitoInfections++;
            //TO DO VIRULENCE: Parasite should mutate its virulence here
            mosquito->setHuman2MosquitoTransmission(true);
        }

    }
}

void Bite::reportInfectionNumbers(ofstream& file)
{
    file << humanInfections<<";"<<mosquitoInfections; // <<";";
}

void Bite :: reportSpreaderNumbers(ofstream& file,double simTime)
{
    file <<simTime<<";" << uninfectedMosquitoes <<";" <<carrierMosquitoes<<";"<<spreadersMosquitoes<<";" <<superSpreadersMosquitoes<<"\n";
}

// Update event class ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void UpdateParameters::execute(Human *v)
{
    //age of humans is in years, and the time step is one day, hence I need to scale it to age
    v->setAge(v->getAge()+Parameters::timeStep/365.0);
    
    //set infection state for humans depending on the parasite development time
    if(v->isExposed())
    {
        if(simTime - v->getExposureTime() >= normalDistribution(Parameters::meanParasiteDevelopmentInHuman, sqrt(Parameters::sdParasiteDevelopmentInHuman)))
        {
            v->setInfectionState("infected");
            v->setParasiteLoad(v->getParasite().getHumanVirulence());
            v->setInfectionTime(simTime);
        }
    }
    
    if(v->isInfected())
    {
        if(simTime - v->getInfectionTime() >= normalDistribution(20.0, 2.25))
        {
            
            if(randomNumberDouble()< v->getRecoveryRate())
            {
                v->clearInfection();
                v->setRecoveryTime(simTime);
            }
            else
            {
                v->setInfectionState("reservoir");
                v->setParasiteLoad(Parameters::parasitemiaReservoir);
            }
        }
    }
}

void UpdateParameters::execute(Female *v)
{
    v->updateParameters();
    
    if(Parameters::metabolism)
    {
        if(v->isExposed())
        {
            double energy = v->getEggEnergy()*(1.0-0.85*v->getParasite().getParasiteValue());
            v->setEggEnergy(energy);
        }
        else{
            v->setEggEnergy(1.0);
        }
    }
    
    if(v->getWaitingTime()>0.0)
    {

        //reset time for biting.
        double timer = v->getWaitingTime();
        if(simTime - timer >= normalDistribution(2.0, 0.25))
        //if (simTime - timer >= immunityTime(2.0,0.05)) //2019-02-27: this waiting time should be a property of the mosquito. Not always 2-3 days. Then in the update event, the mosquito can only become infectious if the time is larger than the waiting time and the incubation period(?). TO DO TEMPERATURE: "Immunity time" should be replaced with a function that is dependent on temperature!
        {
            v->setWaitingTime(0.0);
            v->setMosquitoStage("layingEggs");  //if the timer is done, then the mosquito is ready for oviposition
        }
       /* else
        {
            if(Parameters::metabolism)
            {
                if(v->isExposed())
                {
                    double energy = v->getEggEnergy()*(1.0-0.85*v->getParasite().getParasiteValue());
                    v->setEggEnergy(energy);
                }
                else{
                    v->setEggEnergy(1.0);
                }
            }

        }*/
    }
    if(v->isExposed())//MosquitoExposed())
    {
        
        if(Parameters::metabolism)
        {
            if(simTime - v->getExposureTime() >= 3.0 && simTime - v->getExposureTime() <= v->getParasite().getIncubationPeriodMosquito())
            {
                if(floor(simTime - v->getWaitingTime())>0.0 && floor(simTime -v->getWaitingTime()<2.0))
                {
                    double increment = v->getParasite().getInitialTransmissionProb()+(v->getParasite().getParasiteBM()/(v->getParasite().getParasiteStrength()+v->getParasite().getParasiteBM()));
                    v->getParasite().setParasiteValue(increment);
                    v->getParasite().setParasiteBM(v->getParasite().getParasiteBM()+1.0);
                }
            }
        }
        else
        {
            if(simTime - v->getExposureTime() >= 3.0 && simTime - v->getExposureTime() <= v->getParasite().getIncubationPeriodMosquito())
            {
                if(floor(simTime - v->getWaitingTime())>0.0 && floor(simTime -v->getWaitingTime()<2.0))
                {
                    v->getParasite().setParasiteBM(v->getParasite().getParasiteBM()+1.0);
                }
            }
        }
        
        if(simTime - v->getExposureTime() > normalDistribution(v->getParasite().getIncubationPeriodMosquito(),0))
        {
            v->setInfectionState("infected");
        }
    }
    
    if(v->isRecovered())
    {
        cout <<"Mosquito should not recover from the infection yet!\n";
        exit(9);
    }
    if(v->isReservoir())
    {
        cout <<"Mosquito should never ever be a reservoir!!!!!!!!!!!!\n";
        exit(9);
    }
    
    v->setHuman2MosquitoTransmission(false);
    v->setMosquito2HumanTransmission(false);
}


void UpdateParameters::execute(Larva *v)
{
    v->updateParameters();
}

// Report event class ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ReportParameters::ReportParameters(const string name)
{
    simulationTime = 0.0;
    file.open(name, ios::out);
    file << "Time;ID;Age;Dead;Susceptible;Exposed;Infected;Recovered;Reservoir;ParasiteLoad;ParasiteID;DailyBites\n";
}

ReportParameters::ReportParameters(double _simulationTime, const string name)
{
    this->simulationTime = _simulationTime;
    
    stringstream ss;
    ss << simulationTime;
    
    //set file name specific
    string s(ss.str());
    s+=name;
    
    file.open(s, ios::out);
    file << "Time;ID;Age;Dead;Susceptible;Exposed;Infected;Recovered;Reservoir;ParasiteLoad;ParasiteID;DailyBites\n";
}

void ReportParameters::execute(Human *v)
{
    v->reportParameters(file, simulationTime);
    file <<v->isSusceptible()<<";"<<v->isExposed()<<";"<<v->isInfected()<<";"<<v->isRecovered()<< "; "<<v->isReservoir()<<";"<<v->getParasiteLoad()<<";"<<v->getParasite().getDNA().getDNAID()<<";"<<v->getNumberOfBites()<<"\n";
}


ReportLarva::ReportLarva(const string name)
{
    simulationTime = 0.0;
    file.open(name, ios::out);
    file << "Time;ID;Age;Dead;Adult\n";
}

ReportLarva::ReportLarva(double _simulationTime, const string name)
{
    this->simulationTime = _simulationTime;
    
    stringstream ss;
    ss << simulationTime;
    
    //set file name specific
    string s(ss.str());
    s+=name;
    
    file.open(s, ios::out);
    file << "Time;ID;Age;Dead;Adult\n";
}


void ReportLarva::execute(Larva *v)
{
    v->reportParameters(file,simulationTime);
    file <<v->isAdult()<<"\n";
}


ReportMosquito::ReportMosquito(const string name)
{
    simulationTime = 0.0;
    file.open(name, ios::out);
    //file << "Time;ID;Age;Dead;NumberEggs;WaitingTime;Seeking;Biting;Susceptible;Exposed;Infected;Recovered;TransmissionH2M;TransmissionM2H;ParasiteID\n";
    file << "Time;ID;Age;Dead;NumberEggs;WaitingTime;Seeking;Biting;Susceptible;Exposed;Infected;Recovered;TransmissionH2M;TransmissionM2H;ParasiteID;ParasiteIncubation;Uninfected;Carrier;Spreader;SuperSpreader;NumberBloodMeals;EggEnergy;ParasiteEnergy;ParasiteBM;ParasiteStrength\n";
}


ReportMosquito::ReportMosquito(double _simulationTime, const string name)
{
    this->simulationTime = _simulationTime;
    
    stringstream ss;
    ss << simulationTime;
    
    //set file name specific
    string s(ss.str());
    s+=name;
    //s = dir+s; ->change it according to the output directory
    file.open(s, ios::out);
    file << "Time;ID;Age;Dead;NumberEggs;WaitingTime;Seeking;Biting;Susceptible;Exposed;Infected;Recovered\n";
}


void ReportMosquito::execute(Female *v)
{
    v->reportParameters(file,simulationTime);
    file<< v->getNumberEggs()<<";"<<v->getWaitingTime()<<";"<<v->isMosquitoSeeking()<<";"<<v->isMosquitoBiting()<<";"<<v->isSusceptible()<<";"<<v->isExposed()<<";"<<v->isInfected()<<";"<<v->isRecovered()<<";"<<v->isH2MTrue()<<";"<<v->isM2HTrue()<<";"<<v->getParasite().getDNA().getDNAID()<< ";"<<v->getParasite().getIncubationPeriodMosquito()<<";"<<v->isMosquitoUninfected()<<";"<<v->isMosquitoCarrier()<<";"<<v->isMosquitoSpreader()<<";"<<v->isMosquitosSuperSpreader()<<";"<<v->getNumberBloodMeals()<<";"<<v->getEggEnergy()<<";"<<v->getParasite().getParasiteValue()<<";"<<v->getParasite().getParasiteBM()<<";"<<v->getParasite().getParasiteStrength()<<"\n";
}
