//
//  Parasite.hpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#ifndef Parasite_hpp
#define Parasite_hpp

#include <stdio.h>
#include "DNA.hpp"
#include "Parameters.hpp"

class Parasite
{
public:
    Parasite();
    virtual ~Parasite(){};
    
    double getHumanVirulence(){return this->human_virulence;};
    double getMosquitoVirulence(){return this->mosquito_virulence;};
    void setHumanVirulence(double _virulence){this->human_virulence = _virulence;};
    void setMosquitoVirulence(double _virulence){this->mosquito_virulence = _virulence;};
    
    double getIncubationPeriodMosquito(){return this->incubationPeriodMosquito;};
    void setIncubationPeriodMosquito(double _number);
    
    void setParasiteValue(double _value){this->value = _value;};
    double getParasiteValue(){return this->value;};
    
    void setParasiteBM(int _value){this->bloodmeals = _value;};
    int getParasiteBM(){return this->bloodmeals;};
    
    void setParasiteSrength(double _value){this->strength = _value;};
    double getParasiteStrength(){return this->strength;};
    
    double getInitialTransmissionProb(){return this->p0;};
    void setInitialTransmissionProb(double _value){this->p0 = _value;};
    
    ParasiteDNA& getDNA(){return genotype;};
    Parasite& Copy(Parasite& rhsParasite);
    
    //void resetParasite(bool reservoir);
    void resetParasite();
    void mutateEIP();
    void mutateStrength();
private:
    ParasiteDNA genotype;
    double mutationRate;
    double human_virulence;
    double mosquito_virulence;
    double h2mTransmission;
    double m2hTransmission;
    double incubationPeriodMosquito;
    double value;
    int bloodmeals;
    double strength;
    double p0;
};


#endif /* Parasite_hpp */
