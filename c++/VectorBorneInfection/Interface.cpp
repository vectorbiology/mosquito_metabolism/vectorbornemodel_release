//
//  Interface.cpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 10.01.18.
//  Copyright © 2018 Paola Carrillo-Bustamante. All rights reserved.
//

#include "Interface.hpp"

//This functions generates the directories for the simulation results according to the date: YYYYMMDD
void Output::createResultsDirectory(const string name)
{
    // current date/time based on current system
    time_t now = time(0);  
    tm *ltm = localtime(&now);
    
    // extract various components of the tm structure, i.e., year, month, and day
    stringstream y;
    y << 1900 + ltm->tm_year;
    string year = y.str();
    
    stringstream m;
    m << 1 + ltm->tm_mon;
    string month = m.str();
    
    stringstream d;
    d<< ltm->tm_mday;
    string day = d.str();
    
    string dirName(name);
    
    //set the path of the date and create the directory there
    boost::filesystem::path outPath(dirName + year + month + day);
    
    create_directory(boost::filesystem::path(outPath));
    
    // set the simulation path (as a timeStamp) and create the folder simulations there
    stringstream h;
    h<<ltm->tm_hour;
    string hour = h.str();
    
    stringstream min;
    min<<ltm->tm_min;
    string minutes = min.str();
    
    stringstream s;
    s<<ltm->tm_sec;
    string sec = s.str();
    
    string timeStamp = "/";
    timeStamp+=hour+minutes+sec + "/";
    
    boost::filesystem::path simPath(dirName + year + month + day + timeStamp);
    resultsPath=simPath;
    
    create_directory(boost::filesystem::path(resultsPath));
}


void Input::readParameterFile(const string name)
{
    parametersMap.clear();
    
    options_description description("Parameters");
    // this is for a parameter file without defined sections
     description.add_options()
        ("timeStep", value<double>())
        ("timeEnd", value<double>(), "end of the simulation")
        ("timeInfection", value<double>(), "time starting the infection")
        ("timeMutation", value<double>(), "time starting parasite mutation")
        ("initialInfection", value<bool>(), "should the host population be initialized with a Malaria reservoir population?")
        ("individualEffects", value<bool>(), "should the the random individual effects for larvae be considered?")
        ("metabolism", value<bool>(), "should the within-vector metaolic processes be considered?")
        ("transmissionProbability", value<double>(), "how high should be the probability of transmitting the parasite during a mosquito bite")
        ("seasonality", value<bool>(), "should the temperature profile be considered?")
        ("temperature", value<double>(), "temperature in Celsius (in case no profile is used)")
        ("temperatureFile", value<string>(), "file containing the teperature of a season")
        ("deathRatesFile", value<string>(), "file containing the death rates of adult mosquitoes")
        ("Biomass", value<double>(), "carrying capacity larvae")
        ("eta_Biomass", value<double>(), "random effect carrying capacity larvae")
        ("K", value<double>(), "carrying capacity humans")
        ("a.d", value<double>())
        ("eta_a.d", value<double>())
        ("b.d", value<double>())
        ("eta_b.d", value<double>())
        ("c.d", value<double>())
        ("eta_c.d", value<double>())
        ("m.t", value<double>())
        ("eta_m.t", value<double>())
        ("k.t", value<double>())
        ("eta_k.t", value<double>())
        ("c.t", value<double>())
        ("eta_c.t", value<double>())
        ("larva.growthRate", value<double>())
        ("eta_larva.growthRate", value<double>())
        ("larva.initialPopulation", value<double>())
        ("human.birthRate", value<double>())
        ("human.deathRate", value<double>())
        ("human.initialPopulation", value<double>())
        ("human.recoveryRate", value<double>())
        ("mosquito.deathRate", value<double>())
        ("mosquito.birthRate", value<double>())
        ("mosquito.bitingRate", value<double>())
        ("shape.gompertz", value<double>(), "shape parameter of the Gompertz survival distribution")
        ("rate.gompertz", value<double>(), "rate parameter of the Gompertz survival distribution")
        ("mosquito.initialPopulation", value<double>())
        ("parasite.developmentHuman.mean", value<double>())
        ("parasite.developmentHuman.sd", value<double>())
        ("parasite.developmentMosquito.mean", value<double>())
        ("parasite.developmentMosquito.sd", value<double>())
        ("parasite.parasitemia", value<double>())
        ("parasite.reservoirProbability", value<double>())
        ("parasite.reservoirVirulence", value<double>())
        ("parasite.mutationRate", value<double>())
    ;
    
    std::ifstream settings_file(name , std::ifstream::in );
    store(parse_config_file(settings_file, description), parametersMap);
    notify(parametersMap);

    settings_file.close();
}

/*
void Input::printVariableNames()
{
    cout<< "Parameters loaded from file to be used in ABM-Simulation\n";
    cout<<"[simulation]\n";
    cout << "timeStep\t" <<parametersMap["timeStep"].as<double>() <<endl;
    cout << "timeEnd\t" <<parametersMap["timeEnd"].as<double>() <<endl;
    cout << "timeInfection\t" <<parametersMap["timeInfection"].as<double>() <<endl;
    cout << "file survival rates\t" <<parametersMap["deathRatesFile"].as<string>() <<endl<<endl;
    
    cout<<"[general]\n";
    cout << "Biomass\t" <<parametersMap["Biomass"].as<double>() <<endl;
    cout << "K\t" <<parametersMap["K"].as<double>() <<endl<<endl;
    
    cout<<"[larva]\n";
    cout << "larva.deathRate\t" <<parametersMap["larva.deathRate"].as<double>() <<endl;
    cout << "larva.initialPopulation\t" <<parametersMap["larva.initialPopulation"].as<double>() <<endl<<endl;
    
    cout<<"[human]\n";
    cout << "human.birthRate\t" <<parametersMap["human.birthRate"].as<double>() <<endl;
    cout << "human.deathRate\t" <<parametersMap["human.deathRate"].as<double>() <<endl;
    cout << "human.initialPopulation\t" <<parametersMap["human.initialPopulation"].as<double>() <<endl;
    cout << "human.recoveryRate\t" <<parametersMap["human.recoveryRate"].as<double>() <<endl<<endl;
    
    cout<<"[mosquito]\n";
    cout << "mosquito.birthRate\t" <<parametersMap["mosquito.birthRate"].as<double>() <<endl;
    cout << "mosquito intrinsic death rate\t" <<parametersMap["mosquito.deathRate"].as<double>() <<endl;
    cout << "mosquito.bitingRate\t" <<parametersMap["mosquito.bitingRate"].as<double>() <<endl<<endl;
    
    cout<<"[parasite]\n";
    cout << "parasite.incubationTimeHumans\t" <<parametersMap["parasite.developmentHuman"].as<double>() <<endl;
    cout << "parasite.incubationTimeMosquito\t" <<parametersMap["parasite.developmentMosquito"].as<double>() <<endl;
    cout << "parasite.parasitemia\t" <<parametersMap["parasite.parasitemia"].as<double>() <<endl;
}*/

void Input:: saveVariableMap(variables_map vm, const string fileName, const string dirName)
{
    Output dir;
    dir.createResultsDirectory(dirName);
    string path = dir.getPath();
    path+=fileName;
    
    std::fstream file;
    
    file.open(path, ios::out);
    
    for (variables_map::iterator it = vm.begin(); it != vm.end(); it++)
    {
        file << "> " << it->first;
        if (((boost::any)it->second.value()).empty())
        {
            file << "(empty)";
        }
        if (vm[it->first].defaulted() || it->second.defaulted())
        {
            file << "(default)";
        }
        file << "=";
        
        bool is_char;
        try
        {
            boost::any_cast<const char *>(it->second.value());
            is_char = true;
        }
        catch (const boost::bad_any_cast &)
        {
            is_char = false;
        }
        bool is_str;
        try
        {
            boost::any_cast<std::string>(it->second.value());
            is_str = true;
        }
        catch (const boost::bad_any_cast &)
        {
            is_str = false;
        }
        
        if (((boost::any)it->second.value()).type() == typeid(int))
        {
            file << vm[it->first].as<int>() << std::endl;
        }
        else if (((boost::any)it->second.value()).type() == typeid(bool))
        {
            file << vm[it->first].as<bool>() << std::endl;
        }
        else if (((boost::any)it->second.value()).type() == typeid(double))
        {
            file << vm[it->first].as<double>() << std::endl;
        }
        else if (is_char)
        {
            file << vm[it->first].as<const char * >() << std::endl;
        }
        else if (is_str)
        {
            std::string temp = vm[it->first].as<std::string>();
            if (temp.size())
            {
                file << temp << std::endl;
            }
            else
            {
                file << "true" << std::endl;
            }
        }
        else
        { // Assumes that the only remainder is vector<string>
            try
            {
                std::vector<std::string> vect = vm[it->first].as<std::vector<std::string> >();
                uint i = 0;
                for (std::vector<std::string>::iterator oit=vect.begin();
                     oit != vect.end(); oit++, ++i)
                {
                    file << "\r> " << it->first << "[" << i << "]=" << (*oit) << std::endl;
                }
            }
            catch (const boost::bad_any_cast &)
            {
                file << "UnknownType(" << ((boost::any)it->second.value()).type().name() << ")" << std::endl;
            }
        }
    }
    file.close();
}


void Input :: printVariableMap(variables_map vm)
{
    for (variables_map::iterator it = vm.begin(); it != vm.end(); it++)
    {
        std::cout << "> " << it->first;
        if (((boost::any)it->second.value()).empty())
        {
            std::cout << "(empty)";
        }
        if (vm[it->first].defaulted() || it->second.defaulted())
        {
            std::cout << "(default)";
        }
        std::cout << "=";
        
        bool is_char;
        try
        {
            boost::any_cast<const char *>(it->second.value());
            is_char = true;
        }
        catch (const boost::bad_any_cast &)
        {
            is_char = false;
        }
        bool is_str;
        try
        {
            boost::any_cast<std::string>(it->second.value());
            is_str = true;
        }
        catch (const boost::bad_any_cast &)
        {
            is_str = false;
        }
        
        if (((boost::any)it->second.value()).type() == typeid(int))
        {
            std::cout << vm[it->first].as<int>() << std::endl;
        }
        else if (((boost::any)it->second.value()).type() == typeid(bool))
        {
            std::cout << vm[it->first].as<bool>() << std::endl;
        }
        else if (((boost::any)it->second.value()).type() == typeid(double))
        {
            std::cout << vm[it->first].as<double>() << std::endl;
        }
        else if (is_char)
        {
            std::cout << vm[it->first].as<const char * >() << std::endl;
        }
        else if (is_str)
        {
            std::string temp = vm[it->first].as<std::string>();
            if (temp.size())
            {
                std::cout << temp << std::endl;
            }
            else
            {
                std::cout << "true" << std::endl;
            }
        }
        else
        { // Assumes that the only remainder is vector<string>
            try
            {
                std::vector<std::string> vect = vm[it->first].as<std::vector<std::string> >();
                uint i = 0;
                for (std::vector<std::string>::iterator oit=vect.begin();
                     oit != vect.end(); oit++, ++i)
                {
                    std::cout << "\r> " << it->first << "[" << i << "]=" << (*oit) << std::endl;
                }
            }
            catch (const boost::bad_any_cast &)
            {
                std::cout << "UnknownType(" << ((boost::any)it->second.value()).type().name() << ")" << std::endl;
            }
        }
    }
}

