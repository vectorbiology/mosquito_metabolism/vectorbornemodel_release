//
//  Interface.hpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 10.01.18.
//  Copyright © 2018 Paola Carrillo-Bustamante. All rights reserved.
//

#ifndef Interface_hpp
#define Interface_hpp

#include <iostream>
#include <stdio.h>
#include <ctime>
#include <sstream>
#include <fstream>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

//using namespace boost::filesystem;
using namespace boost::program_options;
using namespace std;

class Interface
{
public:
    Interface(){};
    virtual ~Interface(){};
    void readFile();
    void writeOutput();
};


class Output : public Interface
{
public:
    Output(){};
    virtual ~Output(){};
    
    void createResultsDirectory(const string name); //works
    void createDirectory();
    
    string getPath(){return resultsPath.string();};
protected:
    boost::filesystem::path resultsPath;
};

class Input : public Interface
{
public:
    Input(){};
    virtual ~Input(){};
    
    void readParameterFile(const string name);
    void printVariableNames();
    void printVariableMap(variables_map vm);
    void saveVariableMap(variables_map vm, const string fileName,const string dirName);

    //protected:
    variables_map parametersMap;
};

#endif /* Interface_hpp */
