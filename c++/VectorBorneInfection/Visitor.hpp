//
//  Visitor.hpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#ifndef Visitor_hpp
#define Visitor_hpp

#include <stdio.h>
#include "Individual.hpp"
#include <vector>
#include "Parameters.hpp"

#include <fstream>
#include <sstream>

#define MEANEGGS 55.0
#define SDEGGS 5.0

//#define MEANEGGS 80
//#define SDEGGS 2

using namespace std;



// forward declarations
class Individual;
class Human;

class Female;
class Larva;
class Male;

class Parasite;

class Event
{
public:
 //   virtual void execute(Individual*)=0;
    
    virtual void execute(Human*)=0;
    
    virtual void execute(Larva*)=0;
    virtual void execute(Female*)=0;
    virtual void execute(Male*)=0;
    
    virtual void execute(Parasite*)=0;
    
};


class Death : public Event
{
 
public:
    Death();
    virtual ~Death(){};
    
    
    void execute(Human*);
    
    void execute(Larva*);
    void execute(Female*);
    void execute(Male*){};
    
    void execute(Parasite*){};
    
    void reportDeathNumbers(ofstream& file);
    void getDeathNumbers();
protected:
    int humanDeaths;
    int mosquitoDeaths;
    int larvaDeaths;
    int youngAge;
};


class Birth : public Event
{
public:
    Birth();
    virtual ~Birth(){};
    
    void execute(Human*);
    
    void execute(Larva*){};
    
    void execute(Female*);
    void execute(Male*){};
    
    void execute(Parasite*){};
    
    void reportBirthNumbers(ofstream& file);
    void getBirthNumbers();
protected:
    int humanBirths;
    int mosquitoBirths;
};


class Grow : public Event
{
public:
    Grow(double _time){this->simTime = _time;};
    virtual ~Grow(){};
    
    void execute(Human*){};
    
    void execute(Larva*);
    
    void execute(Female*){};
    void execute(Male*){};
    
    void execute(Parasite*){};
    
    void setSimulationTime(double _time){this->simTime = _time;};
    
protected:
    double simTime;
};


class Bite : public Event
{
public:
    Bite(double _time);
    virtual ~Bite(){};
    
    void execute(Human*){};
    
    void execute(Larva*){};
    
    void execute(Female*);
    void execute(Male*){};
    
    void execute(Parasite*){};
    
    void pickHuman(Human * _human);
    void reportInfectionNumbers(ofstream& file);
    
    void countSpreaders(Female*);
    void reportSpreaderNumbers(ofstream& file,double simTime);
    
protected:
    double simTime;
    Human* human;
    int humanInfections;
    int mosquitoInfections;
    int uninfectedMosquitoes;
    int carrierMosquitoes;
    int spreadersMosquitoes;
    int superSpreadersMosquitoes;
    
    void transmitParasite(Female *v, double transmissionProb);
};

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class UpdateParameters : public Event
{
public:
    UpdateParameters(double _time){this->simTime = _time;};
    virtual ~UpdateParameters(){};
    
    void execute(Human*);
    
    void execute(Larva*);
    void execute(Female*);
    void execute(Male*){};
    
    void execute(Parasite*){};
    
protected:
    double simTime;

};

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class ReportParameters : public Event
{
public:
    ReportParameters(){};
    ReportParameters(const string name);
    ReportParameters(double _simulationTime, const string name);
    virtual ~ReportParameters(){};
    
    void execute(Human*);
    
    void execute(Larva*){};
    void execute(Female*){};
    void execute(Male*){};
    
    void execute(Parasite*){};
    
    void setSimulationTime(double _time){this->simulationTime = _time;};

protected:
    
    Output dir;
    fstream file;
    double simulationTime;
    
};

class ReportMosquito : public ReportParameters
{
public:
    ReportMosquito(const string name);
    ReportMosquito(double _simulationTime, const string name);
    virtual ~ReportMosquito(){};
    
    void execute(Human*){};
    
    void execute(Larva*){};
    
    void execute(Female*);
    void execute(Male*){};
    
    void execute(Parasite*){};
};

class ReportLarva : public ReportParameters
{
public:
    ReportLarva(const string name);
    ReportLarva(double _simulationTime, const string name);
    virtual ~ReportLarva(){};
    
    void execute(Human*){};
    
    void execute(Larva*);
    
    void execute(Female*){};
    void execute(Male*){};
    
    void execute(Parasite*){};

};

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class Visitor
{
public:
    Visitor(){};
    virtual ~Visitor(){};
    
    Visitor(Event* a)
    {
        this->event = a;
    }
    
    void setEvent(Event* b)
    {
        this->event = b;
    };
    
//    void visit(Individual *i){this->event->execute(i);};
    
    void visit(Human *i){this->event->execute(i);};
    
    void visit(Larva *i){this->event->execute(i);};
    void visit(Female *i){this->event->execute(i);};
    void visit(Male *i){this->event->execute(i);};
    
    void visit(Parasite *i){this->event->execute(i);};
    
protected:
    Event* event;
    
};






#endif /* Visitor_hpp */
