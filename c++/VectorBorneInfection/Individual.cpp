//
//  Individual.cpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 24.08.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#include "Individual.hpp"
#include "Parameters.hpp"

Individual :: Individual()
{
    initializeParameters();
}

void Individual :: initializeParameters()
{
    age = generation = 0;
    dead = false;
    infectionTime = 0.0;
    exposureTime = 0.0;
    recoveryTime = 0.0;
}

Individual& Individual :: Copy(Individual& rhs)
{
    
    if(this == &rhs)
        return *this;
    
    this->ID = rhs.ID;
    this->age = rhs.age;
    this->generation = rhs.generation;
    this->dead = rhs.dead;
    this->infectionTime = rhs.infectionTime;
    this->recoveryTime = rhs.recoveryTime;
    this->exposureTime = rhs.exposureTime;
    
    return *this;
}

void Individual::updateParameters()
{
    age+=Parameters::timeStep;
}

void Individual:: reportParameters(fstream& outfile, double _simulationTime)
{
    outfile << _simulationTime<<";"<<ID<<";"<<age << ";"<<dead << ";";
}



void Individual::setInfectionState(const string name)
{
    if(name.compare("susceptible")==0)
        this->infectionState=susceptible;
    else if(name.compare("exposed")==0)
        this->infectionState=exposed;
    else if(name.compare("infected")==0)
        this->infectionState=infected;
    else if(name.compare("recovered")==0)
        this->infectionState=recovered;
    else if(name.compare("reservoir")==0)
        this->infectionState=reservoir;
    else
    {
        cout <<"attempt to assign wrong infection state to the individual!\n";
        exit(9);
    }
}

