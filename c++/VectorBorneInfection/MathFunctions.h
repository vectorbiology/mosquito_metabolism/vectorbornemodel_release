/*
 * MathFunctions.h
 *
 *  Created on: Apr 19, 2011
 *      Author: paola
 */

#ifndef MATHFUNCTIONS_H_
#define MATHFUNCTIONS_H_

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_smallint.hpp>
#include <boost/random/normal_distribution.hpp>

#include <vector>
#include <algorithm>
#include <numeric>

#include <fstream>
#include <sstream>

using namespace std;


int randomNumber(int min, int max);
int randomNumberSmallInt(int min, int max);
double randomNumberDouble();
double randomNumberDouble(int min, int max);

double normalDistribution(double mean, double sqrt_sigma );

double calculateMean(vector<int>& _values); //works
double calculateStandardDeviation(vector<int>& _values); //works


int numberEggs(double mean, double sd); //works -> maybe not so efficient but works

//double calculateBitingRate(double prob, int N);
double calculateBitingRate(double prob, int N, int h);
double calculateAgedependentRecoveryRate(double age, double max);

double getAgeDependentRate(vector<double> _rateVector, int age);
vector<double> readFile(const string _deathRatesFile);

double getDensityDependentDeathRate(double _deathRate, double populationSize, double K);
double getDensityDependentBirthRate(double _birthRate, double populationSize, double K);

double getLarvaGrowthRate(int age, double _growthRate, double larvaPopulationSize, double K, double timePupation); //works
double calculateLarvaDeathRate(double _temp, double _a, double eta_a, double _b, double eta_b, double _c, double eta_c);
double calculateTimePupation(double _temp, double _m, double eta_m, double _k,double eta_k, double _c, double eta_c);

double calculateIndividualEffects(double mean, double eta);
double convertRate2Probability(double rate);//works

vector<double> createMosquitoDeathRates(double shape, double rate);
double calculateGompertzHazard(double shape, double rate, double age);

//age dependent human birth/death rates
vector<double> createHumanDeathRates();
vector<double> createHumanBirthRates();

vector<double> createIncubationPeriod(double _mean, double sd);
#endif /* MATHFUNCTIONS_H_ */

