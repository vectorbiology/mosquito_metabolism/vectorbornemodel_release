//
//  Parameters.cpp
//  VectorBorneInfection
//
//  Created by Paola Carrillo-Bustamante on 08.09.17.
//  Copyright © 2017 Paola Carrillo-Bustamante. All rights reserved.
//

#include "Parameters.hpp"

namespace Parameters {
    
    string outDir = "/Users/u_carrillo/Documents/Projects/Malaria/modeling/ABM/results/";
    
    //general simulation parameters
    double timeStep = 0.0; //one day
    double timeEnd = 0.0;
    double timeOfMutation = 0.0;
    bool initialInfection = false;
    bool individualEffects = false;
    bool metabolism = false;
    
//    string _deathRatesFile = "please provide a file with the survival data";
    string _temperatureFile = "please provide a file with the survival data";


    // general biological parameters
    double BIOMASS = 0.0;  //carrying capacity for larvae
    double eta_BIOMASS = 0.0; //carrying capacity larvae
    double K = 0.0;  //carrying capacity for humans
    
    //Larva parameters
    //coeeficients of the quadratic death rate function and their individual random effects
    double a_d = 0.0;
    double b_d = 0.0;
    double c_d = 0.0;
    double eta_a_d = 0.0;
    double eta_b_d = 0.0;
    double eta_c_d = 0.0;

    //coeeficients of the quadratic time of pupation function and their individual random effects
    double m_t = 0.0;
    double k_t = 0.0;
    double c_t = 0.0;
    double eta_m_t = 0.0;
    double eta_k_t = 0.0;
    double eta_c_t = 0.0;


    double larvaGrowthRate = 0.0;
    double eta_larvaGrowthRate = 0.0;
    double larvaPopulationSize = 0.0; //initialization
    
    //Human parameters
    vector<double> ageHumanDeathRates = createHumanDeathRates();  //age dependent
    vector<double> ageHumanBirthRates = createHumanBirthRates();  // age dependent
    
    double humanBirthRate = 0.0;
    double humanDeathRate = 0.0;
    double humanPopulationSize = 0.0; //initialization
    double maxRecoveryRate = 0.0;
    
    //Mosquito parameters
    double mosquitoDeathRate = 0.0;
    double mosquitoBirthRate = 0.0;
    double bitingRate = 0.0;
    double shapeGompertz = 0.0;
    double rateGompertz = 0.0;
    double mosquitoPopulationSize = 0.0; //initialization
    
    vector<double> ageMosquitoDeathRates;

    //Parasite parameters
    double meanParasiteDevelopmentInHuman = 0.0;
    double sdParasiteDevelopmentInHuman = 0.0; 

    double meanParasiteDevelopmentInMosquito = 0.0;
    double sdParasiteDevelopmentInMosquito = 0.0;
    vector<double> incubationPeriodMosquitoes;
    
    double parasitemia = 0.0;//0.05;
    double reservoirProbability = 0.0;
    double parasitemiaReservoir = 0.0;
    double parasiteMutationRate = 0.0;
    
    //Infection parameters
    double timeToStart = 0.0;
    double transmissionProbability = 1.0;
    
    
    //Environment Temperature
    double temperature = 28.0;
    vector<double> temperatureProfile;
    bool seasonality = false;
}

void Parameters::readConfigFile(const string name)
{
    Input paramFile;
    paramFile.readParameterFile(name);
    
    cout << "Loading Parameter file: " << name <<endl;
    
    //general parameters
    timeStep = paramFile.parametersMap["timeStep"].as<double>();
    timeEnd = paramFile.parametersMap["timeEnd"].as<double>();
    timeToStart = paramFile.parametersMap["timeInfection"].as<double>();
    timeOfMutation = paramFile.parametersMap["timeMutation"].as<double>();
    initialInfection = paramFile.parametersMap["initialInfection"].as<bool>();
    individualEffects = paramFile.parametersMap["individualEffects"].as<bool>();
    metabolism = paramFile.parametersMap["metabolism"].as<bool>();
    transmissionProbability = paramFile.parametersMap["transmissionProbability"].as<double>();
    seasonality = paramFile.parametersMap["seasonality"].as<bool>();
    temperature = paramFile.parametersMap["temperature"].as<double>();
//    _deathRatesFile = paramFile.parametersMap["deathRatesFile"].as<string>();
    _temperatureFile = paramFile.parametersMap["temperatureFile"].as<string>();
    
    // general biological parameters
    BIOMASS = paramFile.parametersMap["Biomass"].as<double>();  //carrying capacity for larvae
    eta_BIOMASS = paramFile.parametersMap["eta_Biomass"].as<double>();  //carrying capacity for larvae
    
    //slighlty change the BIOMASS in every simulation if we include the individual effects
    if(individualEffects)
    {
        BIOMASS = calculateIndividualEffects(BIOMASS, eta_BIOMASS);
    }
    
    K = paramFile.parametersMap["K"].as<double>();  //carrying capacity for humans
    
    //Larva parameters
    //coeeficients of the quadratic death rate function and their individual random effects
    a_d = paramFile.parametersMap["a.d"].as<double>();
    eta_a_d = paramFile.parametersMap["eta_a.d"].as<double>();
    b_d = paramFile.parametersMap["b.d"].as<double>();
    eta_b_d = paramFile.parametersMap["eta_b.d"].as<double>();
    c_d = paramFile.parametersMap["c.d"].as<double>();
    eta_b_d = paramFile.parametersMap["eta_c.d"].as<double>();
    
    //coeeficients of the quadratic time of pupation function and their individual random effects
    m_t = paramFile.parametersMap["m.t"].as<double>();
    eta_m_t = paramFile.parametersMap["eta_m.t"].as<double>();
    k_t = paramFile.parametersMap["k.t"].as<double>();
    eta_k_t = paramFile.parametersMap["eta_k.t"].as<double>();
    c_t = paramFile.parametersMap["c.t"].as<double>();
    eta_c_t = paramFile.parametersMap["eta_c.t"].as<double>();
    
    larvaGrowthRate = paramFile.parametersMap["larva.growthRate"].as<double>();
    eta_larvaGrowthRate = paramFile.parametersMap["eta_larva.growthRate"].as<double>();
    
    larvaPopulationSize = paramFile.parametersMap["larva.initialPopulation"].as<double>(); //initialization
    temperatureProfile = readFile(_temperatureFile);
    
    //Human parameters
    humanBirthRate = paramFile.parametersMap["human.birthRate"].as<double>()*timeStep/365.0;
    humanDeathRate = paramFile.parametersMap["human.deathRate"].as<double>()*timeStep/365.0;
    humanPopulationSize = paramFile.parametersMap["human.initialPopulation"].as<double>(); //initialization
    maxRecoveryRate = paramFile.parametersMap["human.recoveryRate"].as<double>();
    
    //Mosquito parameters
    mosquitoDeathRate = paramFile.parametersMap["mosquito.deathRate"].as<double>();
    mosquitoBirthRate = paramFile.parametersMap["mosquito.birthRate"].as<double>();
    bitingRate = paramFile.parametersMap["mosquito.bitingRate"].as<double>();
//    ageMosquitoDeathRates = readFile(_deathRatesFile);
    shapeGompertz = paramFile.parametersMap["shape.gompertz"].as<double>();
    rateGompertz = paramFile.parametersMap["rate.gompertz"].as<double>();
    ageMosquitoDeathRates = createMosquitoDeathRates(shapeGompertz, rateGompertz);
    mosquitoPopulationSize = paramFile.parametersMap["mosquito.initialPopulation"].as<double>();
    
    //Parasite parameters
    meanParasiteDevelopmentInHuman = paramFile.parametersMap["parasite.developmentHuman.mean"].as<double>();
    sdParasiteDevelopmentInHuman = paramFile.parametersMap["parasite.developmentHuman.sd"].as<double>();
    
    meanParasiteDevelopmentInMosquito = paramFile.parametersMap["parasite.developmentMosquito.mean"].as<double>();
    sdParasiteDevelopmentInMosquito = paramFile.parametersMap["parasite.developmentMosquito.sd"].as<double>();
    incubationPeriodMosquitoes = createIncubationPeriod(meanParasiteDevelopmentInMosquito, sdParasiteDevelopmentInMosquito);
    
    parasitemia = paramFile.parametersMap["parasite.parasitemia"].as<double>();
    reservoirProbability = paramFile.parametersMap["parasite.reservoirProbability"].as<double>();
    parasitemiaReservoir = paramFile.parametersMap["parasite.reservoirVirulence"].as<double>();
    parasiteMutationRate=paramFile.parametersMap["parasite.mutationRate"].as<double>();
    
    paramFile.saveVariableMap(paramFile.parametersMap, "parameters_simulation.txt", outDir);
    
    cout<<"Parameter file successfully loaded and saved!\n Simulation running with the following parameters:" <<endl;
    
    paramFile.printVariableMap(paramFile.parametersMap);
}

void Parameters::printIncubationPeriod(const string path)
{
    //save incubation period profile
    
    ofstream mosquitoIncubation;
    const string mosquitoIncubationFile (path + "IncubationPeriod.csv");
    mosquitoIncubation.open(mosquitoIncubationFile.c_str());
    
    mosquitoIncubation << "IncubationPeriod\n";
    
    for (int i = 0; i <incubationPeriodMosquitoes.size()-1; i++)
    {
        mosquitoIncubation <<incubationPeriodMosquitoes.at(i)<<endl;
    }
    mosquitoIncubation.close();
}
