# vectorBorneModel_release

## Description
This project simulates mosquito population dynamics and malaria transmission as described in the reserach paper [Mosquito metabolism shapes Plasmodium life-history traits](https://www.biorxiv.org/content/10.1101/2022.07.06.498937v1). The modelling framework consists of an individual-based model with an extensive description of mosquito biology. Specifically, this is the first model that includes a description of nutrient allocation inside female mosquitoes and their interactions with growing malaria parasites. 

## Requirements & Installation

The simulation code has been implemented in C++ and relies on libraries from [Boost Library](https://www.boost.org/) version 1.61. The code has been developed on macOS within an Xcode project (included here). For usage on other operating systems, an additional CMake file (not provided here) will be required for compilation.

## Usage
Once compiled, execute the application using the following command in the command line:
```
vectorBorneModel -f parameter_file.txt -o outputDir
```
Each simulation creates a directory with a timestamp, where multiple text files will be saved, each containing characteristics of individual agents over time. On macOS Ventura 13.4.1, a simulation run for 100 simulated days typically takes less than a minute.

Subsequently, the simulated data can be analyzed using scripts written in R version 3.5. For instance, to analyze and visualize population and infection dynamics, use the commands:
```
   RScripts/analyzePopulationSize.R -d outputDir/timeStamp 
   RScripts/plotPopulationSize.R -d outputDir/timeStamp/analysis
```
To analyze the biting behavior, run the command:

```
   RScripts/analyzeBitingRate.R -d outputDir/timeStamp 
   RScripts/plotBitingRate.R -d outputDir/timeStamp/analysis   
```


## Parameter files
The parameter files used are text files with all values described in Table 2 of the referenced manuscript.

## Parameter sweeps and other analysis

This section describes how to conduct multiple stochastic simulations using a pipeline that runs command line scripts in bash and analyzes the results using R. The simulations are performed to study the behavior of a system under different parameter settings.

### Running the Pipeline

To run the pipeline and execute multiple simulations, follow these steps:

1. Open a terminal or command prompt.
2. Navigate to the directory containing the pipeline and scripts.

3. Use the following command:

   ```
   parameterSweep_deathRates.sh -d ./VectorBorneInfection -f parameters_28_metabolism_longEvo.txt -n 10 -l 1
   ```

   In this example, the pipeline will run 10 simulations using the executable "VectorBorneInfection". The parameter file "parameters_28_metabolism_longEvo.txt" will be used for these simulations.

Please ensure that you have the necessary dependencies installed for both bash and R (version 3.5) to execute the pipeline successfully.

By running this pipeline, you can efficiently explore how different parameter combinations affect the system's behavior and gain valuable insights from the analysis of the simulation results.

## Support
If you need support to run, understand the framework, or find a bug, please contact Paola Carrillo-Bustamante (carrillo@mpiib-berlin.mpg.de)
