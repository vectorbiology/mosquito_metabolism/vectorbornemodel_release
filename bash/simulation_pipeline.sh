#!/bin/bash

echo "This script runs and performs the analysis of the ABM! "

#define usage function
usage()
{
    echo "Usage: $0 -d|--dir directory where the results should be saved -e|--exec (default vectorborne) -f|--file parameter file (default ./parameters.txt) -s|--server are the simulations running on the server? (default TRUE) -l|--longevo are these long-term evolution simulations? (default 0)"
	exit 1
}

#initialize the variables
EXEC='vectorBorneInfection'
FILE='./parameters.txt'
EVO=0

#read the variables from the command line
POSITIONAL=()

# call usage() function if filename not supplied
[[ $# -eq 0 ]] && usage

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
-d|--dir)
DIR="$2"
shift # past argument
shift # past value
;;
-e|--exec)
EXEC="$2"
shift # past argument
shift # past argument
;;
-f|--file)
FILE="$2"
shift # past argument
shift # past argument
;;
-s|--server)
SERVER="$2"
shift # past argument
shift # past argument
;;
-l|--longevo)
EVO="$2"
shift # past argument
shift # past argument
;;
*)    # unknown option
POSITIONAL+=("$1") # save it in an array for later
shift # past argument
;;
esac
done

echo "running simulations now with following parameters:"
echo '-------------' DIR = $DIR
echo '-------------' EXEC = $EXEC
echo '-------------' FILE = $FILE
echo '-------------' SERVER = $SERVER
echo '-------------' LONG EVOLUTION = $EVO
echo
echo '------------- starting the simulation -------------'

export DYLD_LIBRARY_PATH=/Users/u_carrillo/Documents/Resources/boost/stage/lib:$DYLD_LIBRARY_PATH

echo $EXEC -f $FILE -o $DIR
$EXEC -f $FILE -o $DIR

#move to the directory where the results are stored
cd $DIR

for i in */; #loop through the date
do
    cd $i

    for j in */; #loop through every repeat
    do
        cd $j
        pwd
        echo '------------- analyzing the results -------------'
        echo

        echo analyzePopulationSize.R -d ./
        analyzePopulationSize.R -d ./

        echo analyzeBitingRates.R -d ./ -e $EVO
        analyzeBitingRates.R -d ./ -e $EVO

        echo plotPopulationSize.R -d ./analysis/ -t 5000 -s $SERVER
        plotPopulationSize.R -d ./analysis/ -t 5000 -s $SERVER

        echo plotBitingRate.R -d ./analysis/ -s $SERVER
        plotBitingRate.R -d ./analysis/ -s $SERVER

        echo calculateParasiteSRI.R -d ./
        calculateParasiteSRI.R -d ./

        cd ../
        pwd
        echo
        echo '-------------> compressing the results .......'
        echo tar -zcvf "${j%/}".tar.gz $j
        tar -zcvf "${j%/}".tar.gz $j

        echo
        echo '-------------> cleaning up ........'
        echo rm -rf $j
        rm -rf $j
    done
done
echo '------------- done! have a nice day :-) -------------'

