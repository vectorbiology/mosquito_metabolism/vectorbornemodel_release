#!/bin/bash
echo "Analysis of the  ABM simulation starting now...exciting!"

usage()
{ 
	echo "Usage: $0 -d|--dir directory with the deathRates files  -l |--longevo are these long evolutionary simulations? (deafult FALSE)"
	exit 1
}
#initialize variables
EVO=0

# call usage() function if directory  not supplied
[[ $# -eq 0 ]] && usage

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -d|--dir)
    DIR="$2"
    shift # past argument
    shift # past value
    ;;
    -l|--longevo)
    EVO="$2"
    shift # past argument
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

echo "reading simulations from:"$DIR

#analyzePopulationSize.R -d $DIR
analyzeBitingRates.R -d $DIR -e $EVO

#plotPopulationSize.R -d $DIR/analysis/ -t 500
plotBitingRate.R -d $DIR/analysis/

#calculateParasiteSRI.R -d $DIR



