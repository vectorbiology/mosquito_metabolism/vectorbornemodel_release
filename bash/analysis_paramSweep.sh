#!/bin/bash
echo "Analysis of the parameter sweep in the ABM simulation starting now...exciting!"

# define usage function
usage(){
    echo "Usage: $0 -d|--dir directory where the results are -l|--longevo are these long-term evolution simulations? (default FALSE)"
    exit 1
}

#initialize the variables
EVO=0

#read the variables from the command line
POSITIONAL=()

# call usage() function if filename not supplied
[[ $# -eq 0 ]] && usage

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -d|--dir)
    DIR="$2"
    shift # past argument
    shift # past value
    ;;
    -l|--longevo)
    EVO="$2"
    shift # past argument
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

#move to the directory
cd $DIR
#loop through every parameter combination
#for i in */;
#do
#    echo $i
#    cd $i
    #loop through every date
    for j in */;
    do
        echo $j
        cd $j
        
        #loop through every repeat
        for k in */;
        do

            echo 'analyzing the simulations from repeat: '$k
            cd $k
            echo analysis_pipeline.sh -d ./ -l $EVO
            analysis_pipeline.sh -d ./ -l $EVO
            cd ../

            # zip the simulation
            echo 'compressing the files...'
            echo tar -zcvf "${k%/}".tar.gz $k
            tar -zcvf "${k%/}".tar.gz $k

        done
        cd ../
    done
#    cd ../
#done
